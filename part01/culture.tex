%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : culture.tex
% 	Modif : jeu. 16 janv. 2014 23:58
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Chapitre{La culture}

\citation{\propre{Pierre}{Desproges}}{L'intelligence et la culture, c'est comme le parachute, quand on n'en a pas, on s'écrase.}{x}

\citation{\propre{Pierre}{Desproges}}{Et puis quoi, qu'importe la culture ? Quand il a écrit Hamlet, Molière avait-il lu Rostand ? Non.}{x}

\citation{\propre{Jean-Claude}{Carrière}}{L'autarcie culturelle et raciale est une marche à la mort. Elle est tout aussi réalisable que son contraire, une culture mondiale uniforme.}{x}

\citation{\propre{Louis}{Scutenaire}}{Quand j'entends le mot culture, je vois des champs, des bœufs, une alouette, une belle fermière.}{x}

\citation{\propre{Romain}{Guilleaumes}}{L'enseignement produit surtout des individus qui se croient instruits et cultivés lorsqu'ils ne sont que conditionnés et endoctrinés.}{x}

\section{Quelle culture ?}

La notion de culture est très commune dans le langage courant.
Tout le monde la comprend dès qu'il en est question.
Cependant, elle désigne des notions différentes en fonction du contexte.
Parler de la culture avec un paysans, un présentateur de jeu télévisé ou un ethnologue entrainera des discussions très différentes.
Le mot sera le même, mais sa compréhension sera différente.
Chacun comprend ce mot, mais sa définition sera délicate, les notions de cultures étant relativement floues.

Pour enlever une confusion, lorsque je parlerai de la culture qui intéresse le paysan, j'utiliserai le terme «~agriculture~» dans la mesure du possible.
Reste deux sujets à différentier.
Qu'est-ce que la culture d'un présentateur de jeu télévisé ?
C'est une «~culture triviale poursuite~».
C'est une culture dans laquelle les mots sont sortis de leur contexte et sans aucun intérêt.

\begin{itemize}
\item \Index{Marignan} ?
\item \Annee{1515} !
\item Super ! Vous avez gagné la machine à laver 10 vitesses !
\item Génial ! J'en ai rêvé toute ma vie !
\item Je vous comprends ! Vous savez à quoi correspond Marignan ?
\item Euh\ldots non\ldots
\item C'est pas grave ! Moi non plus !
\end{itemize}

Qui défini ce qui appartient à la culture ?
Je ne le sais pas, il n'y a pas de définition claire, mais les gens semblent globalement d'accord sur ce qui doit y appartenir ou pas.
Je n'aime pas le choix commun, je trouve même que c'est un problème grave.
Je ne suis pas le seul à remarquer ce manque, je lis de plus en plus de livres dans lesquels les auteurs s'inquiètent.

Je vais la faire différemment.
Qui sait que $cos(\pi/3)=1/2$ ?
Peu de monde en fait.
Peu de monde sait à quoi correspond le cosinus, alors à plus forte raison, certaines valeurs particulières sont encore moins connues.
Qui considère que ça appartient à la culture ?
J'aurais tendance à dire : strictement personne.
Ni les mathématiciens qui savent à la fois ce qu'est un cosinus et qui connaissent cette valeur.
Ni les autres personnes, beaucoup plus nombreuses, qui ne trouvent même pas que c'est un manque de ne pas savoir ça.
J'ai même l'impression que c'est considéré comme une qualité d'avoir oublié ce qu'est un cosinus.

Dire «~J'ai toujours vécu sans avoir jamais su que \Index{Marignan} c'est \Annee{1515}, je ne m'en porte pas plus mal.~» fait passer pour un cuistre.
Dire «~J'ai toujours vécu sans avoir jamais su que $cos(\pi/3)=1/2$, je ne m'en porte pas plus mal.~» fait passer pour quelqu'un qui a de la répartie.
Pourquoi ?

Parce que, pour être honnête, il est beaucoup plus utile de savoir que $cos(\pi/3)=1/2$ que de savoir que \Index{Marignan} a eu lieu en \Annee{1515}.
Il est possible de vivre sans connaître la signification d'un cosinus.
Mais pour découper un gateau en six, je ne vois pas de moyen qui soit à la fois plus simple et plus précis que la formule ci-dessus.
Mais expliquer la façon de découper un gateau en six à une tablée va casser l'ambiance.
Ce que je considère grave, n'est pas que peu de monde connaisse le $cos(\pi/3)$, il n'est pas vital d'être capable de couper un gateau en six.
Ce que je considère grave est que la totalité de la physique est exclue de la culture.
Des pans entiers de notre compréhension de ce qui nous entoure sont exclus du domaine culturel.

La culture concerne toujours des domaines littéraires.
Lorsque ce sont des sciences, seules les sciences sociales ont droit de cité parmi la culture.
Nous pouvons éventuellement rajouter une partie de la biologie.
Si vous prenez la philosophie, \propre[y]{Emmanuel}{Kant} est considéré comme un grand philosophe (même s'il est un peu dépassé).
Par contre \propre[y]{Karl}{Popper} est relégué dans l'épistémologie, loin des philosophes.
Personne n'oserait considérer \propre[y]{Albert}{Einstein} comme un philosophe.
Pourtant, avec ses découvertes, il a dû se plonger dans la philosophie.
Les philosophes officiels étant cantonnés à essayer de comprendre ce qu'il disait.
Sans la moindre possibilité de le suivre ou de le contredire.
Ceux qui le contredisaient étaient seulement des scientifiques.
À cause de la période, il a eu des adversaires non scientifiques, mais ce n'est pas à ses écrits qu'ils s'en prenaient.

Ce que je trouve grave, c'est que les gens se sentent concernés par les \Index{OGM} ou par le \Index{nucléaire} sans avoir la moindre idée de ce que c'est.
Lorsque j'ai vu la retranscription d'une partie d'un débat télévisé entre un philosophe Français populaire et un président de la république, ça m'a vraiment énervé.
Le philosophe était de gauche, donc il considérait que les gènes n'ont pas d'influence sur le comportement, c'est l'environnement qui fait tout.
Le président était de droite, donc il avait l'opinion inverse.
Je trouve que c'est vraiment un grave problème.
Les gènes ne sont ni de droite ni de gauche.

Le philosophe va donner ses conclusions aux Français alors que les bases de son raisonnement sont mauvaises.
En étant populaire, il est écouté et des gens vont vouloir le suivre alors que ses idées sont nulles.
Le président, ce qui est encore plus grave, va prendre des décisions politiques et va influer la vie des Français sans aucune capacité à analyser correctement le problème.

Avant de vouloir avoir une opinion, il faut savoir comment ça marche.
Ou plutôt, c'est préférable lorsque l'on cherche à la faire partager.
Cela évite de passer pour un cuistre.
Mais avec la science, l'inculture est telle qu'il est possible de raconter n'importe quoi sans être contredit.

L'illettrisme scientifique parmi l'élite intellectuelle est un grave problème.
Le problème est que les élites n'ayant aucune idée de leurs lacunes vont prendre des décisions et donner des conseils sans fondement.
Qu'une personne soit malhonnête ne me gêne pas.
Mais qu'une personne donne des conseils, ou prenne des décisions, stupides juste par cuistrerie me gêne.
Ne pas connaître ses lacunes n'incite pas à se renseigner, mais donne l'impression d'avoir raison.
Cette élite, ne s'intéressant pas à la science, reste dans ses idées préconçues et dépassées.
La science évolue bien plus vite que les connaissances de nos dirigeants.

Le problème vient aussi des scientifiques qui n'essayent pas assez souvent de vulgariser leurs découvertes pour être compris du plus grand nombre.
Je ne vais pas faire une revue scientifique, mais quand je l'estimerai nécessaire, j'aborderai les points scientifiques.
Nous utilisons des ordinateurs, des téléphones portables, des GPS, les yeux sont guéris avec des interventions au laser.
Les GPS ne fonctionnent que grâce à la compréhension de la relativité.
Les téléphones portables ne peuvent être construits que grâce à la compréhension de la physique quantique : c'est la physique quantique qui permet de miniaturiser les transistors contenus dans leurs puces.
Vos téléphones portables sont beaucoup plus puissants que les ordinateurs qui tenaient difficilement dans un hangar il y a cinquante ans.
Nous sommes dans un monde où la science moderne est omniprésente.
Il ne s'agit pas seulement de masturbation intellectuelle pratiquée par des gens très intelligents vivants en reclus dans leur tour d'ivoire.
Je ne peux pas la rejeter de ma culture.

\section{La culture pour un ethnologue}

Donc, vous avez dû le comprendre, la «~culture triviale poursuite~» ne m'intéresse pas.
Vous avez même pu remarquer un certain mépris pour cette culture.
N'allez pas vous imaginer que je méprise ceux qui s'intéressent à cette culture.
Ce que j'essaye de montrer dans ce livre, c'est que c'est la richesse d'influences diverses qui ont fait ma civilisation.
Je ne voudrais surtout pas, maintenant que ma culture (donc celle de mes ancêtres) a atteint un certain niveau, l'enfermer.
Il est important que des gens différents s'intéressent à des sujets différents.
Ce serait dommage que la culture stagne parce que tout le monde est d'accord sur les mêmes sujets.
Je ne m'intéresse pas à la culture «~triviale poursuite~», je n'ai pas envie d'apprendre \Nombre{15000} dates par cœur.
Je ne veux pas décourager ceux qui s'y intéressent.

Ici, je vais parler de la culture telle qu'un ethnologue la conçoit.
C'est à dire une culture de groupe.
Cette culture, c'est quelque chose qu'un homme apprends par son appartenance à un groupe.
Par exemple, lorsque je mets un aliment dans ma bouche pour me nourrir, il n'y a rien de culturel.
Le fait d'être un être humain m'oblige à mettre des aliments dans ma bouche pour me nourrir.
Mais, la préparation de cet aliment, son mélange avec d'autres aliments, sa cuisson et l'utilisation d'un couteau et d'une fourchette pour manger est culturel.
C'est parce que je suis Français que je choisis certaines recettes et pas d'autres.
Si j'étais Chinois, je choisirais d'autres recettes et j'utiliserais des baguettes pour manger.
Bon, quand je vais dans un restaurant chinois ou japonais, j'utilise des baguettes, mais ce n'est pas le sujet.
Ne cherchez pas à tout embrouiller, merci.

Un autre exemple, l'un des faits culturels les plus importants est le \Index{feu}.
Il n'est pas naturel de produire du feu, ce sont des techniques très compliquées qui se sont transmises de génération en génération.
Je ne parle pas de frapper deux silex l'un contre l'autre, ça ne marche que dans les films.
Frotter deux morceaux de bois est plus efficace, mais pas instantané non plus.
Il a fallu trouver une technique.
Une fois la technique trouvée, il a fallu l'expliquer pour que d'autres l'utilisent.
Et que les autres puissent transmettre cette technique à leur tour.
Bref, la culture est basée sur la communication.

\section{La communication}

Cela n'a l'air de rien, mais la communication est primordiale.
Pas seulement pour se sentir moins seul : la vie est basée sur la communication.
Il est possible d'argumenter et de considérer ou refuser que les atomes sont basés sur la communication.
Les grandes avancées dans la communication ont entrainé des changement radicaux dans la vie. 
La vie, la reproduction, la transmission des gênes, le développement des cellules par recopie de l'ADN est basé de façon fondamentale sur la transmission de l'information, bref, sur la communication.
Avant de naître, en nous transmettant leurs gènes, nos parents nous communiquent nos premières informations.
De façon totalement inconsciente, mais vitale.
Cela nous semble tellement naturel, que nous ne le réalisons même pas.
Ce sont nos parents qui nous communiquent la couleur de notre peau.
Mais cette communication fait partie de l'hérédité, pas de la culture.
Une information transmise par les gènes ne peut pas être changée.
Je ne parle pas de chirurgie esthétique et je parlerai de l'épigénétisme plus tard, c'est un peu particulier.

La culture, c'est une transmission volontaire.
Cela semble simple dit comme ça, mais il y a quand même de grands débats sur le sujet.
Ce qui est inné ou acquis prête à discussion pour beaucoup de sujets parmi les savants.
Pas pour la couleur de la peau, ni pour la langue.
Mais pour certains autres traits de caractères.
L'environnement influe sur les gênes, mais ce n'est pas de la culture.
S'il y a une possibilité de discussion sur un caractère inné ou acquis, c'est que ce n'est pas un fait culturel.
En général, ce serait trop simple si une définition pouvait être aussi claire.

Un bébé qui est adopté à l'autre bout du monde apprendra la langue de ses parents adoptifs.
Pas celle de ses parents héréditaires.
Ou plutôt, pas obligatoirement.
La langue fait partie de la culture, c'est même le moyen de transmettre le reste de la culture le plus commun.
C'est d'ailleurs avec la langue que la culture apparaît.

La première révolution dans la communication est le \Index{langage} articulé, je m'y attarderai longuement.
C'est passionnant, vous verrez.
C'est le moyen le plus efficace de transmettre l'information à un proche.
La seconde révolution a été l'invention de l'\Index{écriture}, je m'y attarderai aussi longuement plus tard.
C'est aussi passionnant, ne soyez pas pressés.
L'écriture a permis d'augmenter le nombre d'auditeurs, dans le temps et dans l'espace.
Mais l'écriture a aussi aidé les penseurs à structurer leur pensée.
La troisième révolution est l'invention de l'\Index{imprimerie}, je n'en parlerai pas, c'est pour ça que j'en parle maintenant.
L'\Index{imprimerie} n'a pas changé les mentalités, mais a permis une meilleure diffusion du savoir.
En diffusant le savoir, la connaissance peut progresser de manière plus importante.
L'imprimerie a aussi aidé à diminuer les erreurs de recopie.
C'était donc un énorme apport, par effet de bord.
Ce n'est pas l'imprimerie qui a fait évoluer la science.
Mais l'imprimerie a permis à la science d'évoluer plus vite par une meilleure diffusion des nouvelles idées.

La quatrième révolution est le \Index{télégraphe}, je n'en parlerai pas non plus, je dois donc en parler ici.
Pas le \Index{télégraphe} français par signaux, mais le \Index{télégraphe} électrique, bien connu des amateurs de westerns, qui utilisait le code morse.
Il a diminué l'espace de manière impressionnante.
Il a changé la vision du temps par les gens.
J'emploie ici le mot «~temps~» dans deux de ses sens.

Au niveau météorologique, en connaissant le temps, qu'il faisait simultanément à plusieurs endroits différents a permis de mieux comprendre son évolution.
L'intérêt n'est pas spécialement de savoir qu'il pleut toujours en Bretagne.
Mais c'était l'ancêtre, en moins précis et moins objectif de nos photos satellites.
À la météo, vous voyez les nuages qui se déplacent sur la France et vous en déduisez où ils seront demain.
Le télégraphe était moins précis, car il est plus dur de savoir où comment le nuage et où il finit quand vous le regardez.
Il était moins objectif, car dire que le nuage est gros ou petit varie en fonction des gens.
Comme sa couleur, plus ou moins grise.
Il n'y avait pas de façon de les mesurer à l'œil nu à l'époque.

Au niveau horaire, les gens ont réalisé que le temps est lié à un lieu.
Les astrologues savaient depuis longtemps que le soleil éclairait différents endroits de la terre à différents instants.
Cependant, pour le commun des mortels, le crépuscule, l'aube ou midi étaient des référentiels absolus.
Lorsque le soleil est au plus haut du ciel, il est midi.
C'est une chose que nous savons, mais qui n'allait pas de soit à l'époque.

Le difficulté du décalage horaire n'existe que depuis l'utilisation des avions.
Lorsqu'il fallait des mois pour aller à l'autre bout du monde, rien ne laissait présager que midi en \propre[n]{x}{France} n'avait pas lieu en même temps en \propre[n]{x}{Chine}.
À l'époque, lorsqu'un marin se réveillait sur son bateau, il pouvait se dire que sa femme allait bientôt se lever.
Sans s'imaginer qu'il y avait des heures d'écarts entre le réveil effectif de sa femme et le moment où il croyait qu'elle se levait.
Si je veux parler au téléphone avec un américain, je dois me demander l'heure qu'il est là-bas avant de l'appeler.
Lorsque je veux lui envoyer une lettre, je ne me pose pas la question.
Le \Index{télégraphe} a donc changé les mentalités.

Enfin, la dernière révolution est l'\Index{informatique}.
C'est le traitement automatique de l'information, cela permet des utilisations dans la vie courante qui n'étaient pas imaginables avant.
Je n'en parlerai pas non plus, son impact a plutôt été orientée vers l'accélération des technologie que vers le changement de mentalités.
Ou pas autant, maintenant, une personne va faire ses courses sans se poser de question.
Il n'y a plus la marque de lait habituelle, laquelle prendre ?
Vite, je téléphone à la maison pour le savoir.
Avant, il fallait prendre une grosse décision en choisissant soi-même.
Les conséquences pouvaient être catastrophiques.
La communication est à la base de notre vie, sur ses cinq révolutions, trois ont été jusqu'à changer les mentalités.
La communication est donc vitale, je devrai donc m'y intéresser.

Je n'ai pas parlé des signaux de fumée des amérindiens ni des tam-tam africains.
Ils ne sont pas négligeables.
Le tam-tam africain était un vrai langage de communication très intéressant.
Les signaux de fumés indiens étaient très limités, mais astucieux.
Mais ils n'ont pas influencés ma culture.
De plus, ils sont trop écartés des moyens utilisés par mes ancêtres pour que j'ai le temps de les étudier.
Mais si vous avez des ancêtre africains, intéressez-vous au tam-tam.
C'était un moyen très astucieux, basé sur une particularité de certaines langues africaines.
Vous n'avez pas besoin d'avoir des ancêtres africains pour vous y intéresser.
C'est intéressant, c'est juste que ça déborde trop de mon sujet.

\section{Définition}

La \Index{culture}, telle que j'en parlerai dans ces pages, ce sera l'ensemble des caractéristiques non héréditaires qui définissent un groupe.

Cette définition est très floue, j'en suis conscient.
Mais c'est important, plusieurs cultures peuvent se mêler.
La culture européenne est différente de la culture asiatique.
Mais en même temps, la culture française tout en étant européenne est différente de la culture anglaise.
Dans mon infinie bonté, j'accepte de considérer que les anglais ont une culture. 
dans le même temps, il y a une certaine culture méditerranéenne, à laquelle appartiennent le sud de la \propre[n]{x}{France} et l'\propre[n]{x}{Égypte}.
Le problème est que l'histoire linéaire présentée par les manuels, avec l'antiquité, le moyen-âge, le renaissance et autres, est fondamentalement mauvaise.
Bien sûr, ces repères sont utiles pour avoir un langage commun, mais il sont centrés sur l'histoire européenne.
C'est un non-sens de vouloir y faire rentrer toutes les autres cultures. 
Parler du moyen-âge africain ou chinois est source de confusion et n'apporte rien.
L'histoire n'est pas une avancée linéaire, mais un ensemble de cultures qui ont évolué à des rythmes différents, dans des directions différentes et qui se sont mêlées dans des interactions complexes.
Il est nécessaire de rester vague dans la définition de la culture.
