# L'histoire pour mes grands-mères

Ce projet est destiné à produire un document LaTeX.

## Raison

J'ai lu beaucoup, et il y a un moment où j'ai envie de savoir où j'en suis au niveau de ma connaissance de l'histoire.
Ce document/livre est donc d'abord pour moi.
Je veux savoir quels sont les sujets que je maîtrise, quels sont ceux que j'ai besoin d'approfondir ou de découvrir.
La meilleure façon de le savoir est de le rédiger.
Je laisse les autres le lire mais sans aucune garantie sur les erreurs ni sur le temps qu'il me faudra pour le finir.
Je ne suis pas un historien, c'est un sujet qui m'intéresse mais ce n'est pas l'œuvre de ma vie.
J'avance quand j'en ai envie et quand j'ai du temps.

## Lecture du document compilé
Le document compilé est disponible ici : [histoire.pdf](https://scarpet42.gitlab.io/histoire/histoire.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
