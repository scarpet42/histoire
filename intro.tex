%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
%  Fichier : intro.tex
%    Modif : sam. 14 sept. 2013 12:28
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Chapitre{Introduction}

\citation{-}{Un peuple qui oublie son passé se condamne à le revivre.}{Cette citation a été attribuée à beaucoup de monde, entre autre \propre[y]{Winston}{Churchill} et \propre[y]{x}{Gandhi}}

\citation{\propre{Claude}{Bernard}}{Ceux qui ont une foi excessive dans leurs idées ne sont pas bien armés pour faire des découvertes.}{x}

\citation{\propre{Alphonse}{Allais}}{Ce sera l'inéluctable loi de l'humanité d'avoir toujours quelque chose à ne pas comprendre.}{x}

\citation{\propre{John}{Tyndall}}{It is as fatal as it is cowardly to blink facts because they are not to our taste.}{il est aussi fatal qu'il est lâche d'écarter des faits parce qu'ils ne sont pas à notre goût}

\citation{\propre{Albert}{Einstein}}{You do not really understand something unless you can explain it to your grandmother.}{vous ne comprenez pas vraiment quelque chose tant que vous n'êtes pas capable de l'expliquer à votre grand-mère}

\citation{\propre{Joseph}{Joubert}}{Le but de la discussion ne doit pas être la victoire, mais l'amélioration.}{x}

La question «~Quel est l'intérêt de connaître l'histoire ?~» reçoit invariablement «~Comprendre le présent.~» comme réponse.
Si je suis tout à fait d'accord avec cette façon de voir, il faut admettre que la compréhension du présent par la connaissance de l'histoire n'est pas aussi instantanée que ce qu'il peut sembler.
Prenons un exemple concret.
Qui peut me dire à quoi correspond \Annee{1515} ?
Je suppose que tout le monde sait que c'est \Index{Marignan}.
Maintenant, qui peut dire ce qu'est \Index{Marignan} ?

En dehors de la réponse \Annee{1515}, il y a déjà moins de monde à pouvoir répondre.
Cela signifierait que \Index{Marignan}, en dehors de la simplicité à retenir sa date n'a pas d'importance ?
Pour répondre à cette question, nous n'allons en retenir ici que ce qui est important dans cette introduction.
C'est une bataille que les Français ont livré aux Suisses pour la possession du Milanais.
Les Français ont gagné (j'ai bien précisé que je simplifiais).
La conséquence majeure est que, depuis cette bataille, la Suisse n'a plus jamais fait la guerre (je parle de l'état, pas de mercenaires de cette nationalité).
D'un point de vue militaire, cette bataille est donc la plus belle victoire de tous les temps.

Dans l'histoire du monde, aucune autre victoire n'a autant dégoûté un peuple au point qu'il arrête de faire la guerre pendant un demi millénaire.
Mais surtout, l'importance de cette bataille est que la politique de la Suisse a été profondément modifiée.
Le système bancaire suisse actuel et pendant la seconde guerre mondiale est issu de cette bataille.

Bien sûr, la conséquence peut sembler (à juste titre) un peu simpliste.
Il n'y avait aucun système bancaire au sens actuel à l'époque.
La seule monnaie d'échange internationale était l'or.
Cependant, une fois que les Suisses sont devenus neutres, leur armée n'avait plus la même fonction.
Ils ont pu réutiliser le budget initialement destiné à une armée offensive.
Plus tard, lorsque d'autres armées sont devenues plus belliqueuses, ils ont dû avoir d'autres arguments pour pouvoir rester neutres.
Si le lien avec \Index{Marignan} n'est effectivement pas aussi direct que ce qui est écrit au dessus, il existe néanmoins.

Ce que cet exemple veut surtout illustrer, c'est que si tout le monde sait que \Annee{1515} correspond à \Index{Marignan}, peu de gens savent à quoi cette date correspond.
Le nombre de personnes à connaître les impacts de cette date est donc encore plus faible.

Il n'y a pas si longtemps que ça, je faisais partie de la majorité des gens de ce point de vue là.
Les raisons sont multiples.
La raison principale, c'est que pendant toute ma scolarité je considérais que toutes les matières sans lien direct avec les maths étaient sans intérêt.
La seconde raison, c'est que les matières sont enseignées de façon à connaître des dates, mais la réflexion sur les conséquences des événements est souvent minime.
Mes centres d'intérêts ont évolué, j'ai cherché à mieux connaître mon histoire.
Cependant, les livres d'histoires sont généralement comme le programme scolaire : ils comportent peu de réflexions sur les évènements.
J'ai dû lire beaucoup de livres pour augmenter à la fois ma connaissance et ma compréhension de l'histoire.
Au bout d'un moment, il me devient utile de faire un bilan de mes connaissances pour connaître mes lacunes.

Je n'ai jamais trouvé un livre qui contienne vraiment tout ce qui m'intéresse.
Les livres généraux n'analysent pas assez les évènements à mon goût.
À l'exception d'auteurs exceptionnels, comme ceux que j'ai remerciés dans le préambule, qui malheureusement ne couvrent pas tous les sujets qui m'intéressent.
Les livres plus spécialisés ont plus de réflexions, mais ces réflexions sont très éparpillées dans beaucoup de livres.
De plus, il est intéressant de lire plusieurs livres parlant du même sujet de façon à savoir si l'idée de l'auteur est répandue ou originale.
L'intérêt de lire plusieurs ouvrages aide aussi à savoir si une idée qui nous semble bonne dans un livre est réfutée ailleurs.

Vous pouvez vous demander pour quelle raison je tiens absolument à voir des explications dans les livres.
Le problème, c'est qu'il y a surtout un étalage de faits sans recul.
C'est pourtant important le recul pour savoir si ce qui nous est dit peut-être vrai.
Par exemple, prenons le mot «~anticonstitutionnellement~».
Ce mot, vous en avez bien sûr entendu parler, il paraît que c'est le mot le plus long de la langue française.

Prenons un autre mot, «~mère~» par exemple.
C'est un vrai mot, tout le monde est d'accord là dessus.
Prenons maintenant le mot «~grand-mère~».
Au hazard.
C'est aussi un vrai mot.
Si c'étaient deux mots différents, il faudrait accorder «~grand~» avec «~mère~» ce qui donnerait «~grande mère~».
De plus, il est dans le dictionnaire en tant que tel.
Enfin, il ne prend tout son sens que composé.
Il n'est pas possible de deviner la signification de «~grand-mère~» en regardant les significations de «~mère~» et de «~grand~».

Maintenant, considérons le mot «~arrière-grand-mère~» qui est aussi un vrai mot pour les mêmes raisons que le mot «~grand-mère~».
Je suppose que vous voyez où je veux en venir.
Ce mot est plus petit, il ne fait que 16 lettres alors que «~anticonstitutionnellement~» en fait 26.
Mais je peux continuer à loisir à rajouter autant d'«~arrière~» que ce que je veux jusqu'à ce qu'il soit plus long.
En dehors du fait qu'à partir d'un certain nombre d'«~arrière~» le mot n'est plus dans le dictionnaire, les raisons précédentes sont toujours valables.
Il est possible de parler d'une «~arrière-arrière-arrière-grand-mère~» qui est un vrai mot que tout le monde comprend et qui contient plus de lettres que le mot le plus long de la langue française. 

Alors, que faut-il en déduire ?
Que «~anticonstitutionnellement~» est un plus vrai mot que «~arrière-arrière-arrière-grand-mère~» qui est un néologisme ?
Non, la seule déduction logique est que le mot «~anticonstitutionnellement~» n'est pas le mot le plus long de la langue française.
Il n'y a pas de mot plus long que tous les autres dans la langue française.
Il y a un mot plus long que tous les autres dans un dictionnaire (qui contient forcément un nombre fini de mots), mais il n'est pas pour autant le mot le plus long de la langue française.

Il ne faut pas répéter ce qui précède à un professeur de français, un grammairien, ou pire encore un membre de l'Académie Française.
Pour ces catégories de personnes, la conception du français n'est pas de savoir comment il est parlé, mais comment il devrait être parlé (là, je suis gentil, dans certains cas, j'ai l'impression d'une analyse passéiste plus que d'une analyse rigoureuse).
Si vous en parlez à un linguiste, vous aurez peut-être un écho plus positif.
Honnêtement, je ne peux pas parler à la place des linguistes, je n'en suis pas un.
À ma connaissance, aucun d'entre eux n'a remis en cause ce concept de mot le plus long de la langue française.
Ils ont autre chose à faire, je les comprends.

C'est l'excellent livre «~The Language Instinct~» du linguiste et psychologue \propre[y]{Steven}{Pinker} qui m'a donné cette idée.
Il expliquait comment composer un mot en anglais d'une autre manière, en rajoutant des préfixes et des suffixes.
Un peu à la manière du mot «~constitution~» qui peut donner «~constitutionnel~», puis «~constitutionnellement~» et finalement «~anticonstitutionnellement~».

Je me suis demandé si, en français, il était possible de jouer à la même chose.
Un mot ne peut être considéré comme français que s'il est validé par l'Académie Française.
Comme ces personnes ne peuvent valider une infinité de mots, ils vous expliqueront que j'ai tort.
Par exemple, ils veulent régenter le vocabulaire employé en informatique, mais aucun d'eux ne s'aviserait d'émettre un avis sur le vocabulaire utilisé en biologie.
Pourtant, avec le nombre de mots choisis pour nommer les milliers d'insectes, de plantes, de champignons, de bactéries et autres, ils auraient de quoi s'amuser.
Il y a tellement de mots en biologie que certains spécimens sont désignés par plusieurs mots.
Je ne parle pas seulement du nom savant et du nom commun.
Mais de spécimens qui ont été découverts par un chercheur qui ignorait l'existence d'une découverte antérieure.

Je garde donc ma conclusion pour moi, n'allez pas la répéter si vous ne voulez pas passer pour des cuistres.
Les gens préfèrent avoir confiance dans ce que disent les personnes sérieuses que dans les divagations d'un illuminé dans mon genre.

Ce n'est pas parce que quelque chose est admis par tout le monde que c'est vrai.
Par exemple, à une certaine période, tout le monde admettait que la terre était plate.
C'est pour ça que j'ai besoin de voir des explications dans un livre.
J'ai besoin de savoir si je peux préférer une façon de voir à une autre.
J'ai besoin de savoir si ma connaissance repose sur quelque chose de solide.
J'ai besoin de savoir si lorsque j'apprends un fait nouveau, il remet en question ma connaissance.

Les évènements présentés ici seront vrais (sauf erreur, mais je ne crois pas qu'il puisse y en avoir).
Mais leur interprétation sera personnelle (par exemple, quand je considère que \Index{Marignan} est la plus belle bataille de tous les temps).
D'ailleurs en parlant d'erreur, si vous avez assez de temps à perdre pour me lire et que vous voyez des erreurs, vous pouvez me les remonter.
N'étant ni un chercheur, ni un historien, ni rien dans le style, la quasi totalité des interprétations est déjà disponible dans d'autres ouvrages.
Je suis d'accord avec certains auteurs et en opposition avec d'autres, ce sera ma vision qui sera affichée ici (en général, elle va dans le sens de la majorité des auteurs).
Vous pouvez estimer que ma façon de voir un lien entre \Index{Marignan} et le système bancaire suisse est farfelue.
Vous pouvez aussi continuer à considérer que «~anticonstitutionnellement~» est le plus long mot de la langue française.
Au moins, je vous ai présenté mes arguments, vous pouvez estimer vous-même s'ils vous conviennent.
Si vous les rejetez, ce sera en toute connaissance de cause.
C'est important, c'est la seule façon que j'ai de m'assurer que je comprends ce que j'écris.
Je veux m'assurer que je ne me contente pas de recopier quelque chose que j'aurais entendu quelque part sans le vérifier.

Mon but n'est pas de faire un étalage de dates, mais d'établir ma compréhension du monde.
Les dates et noms de rois et de batailles seront rarement présents.
D'ailleurs, je parlerai rarement des batailles, elles ont souvent peu d'intérêt dans ma recherche.
Par exemple, si la bataille de \Index{Marignan} avait été gagnée par les Suisses, ils auraient probablement encore une armée offensive.
Leur système bancaire serait probablement différent.
Cela aurait changé la vie des Suisses ainsi que nos rapports avec la Suisse.
La Suisse serait peut-être même dans la CEE.
Bref, cette bataille a eu beaucoup d'influence sur la vie des Suisses et des gens du reste du monde dans une moindre mesure.

Cependant, cette bataille n'a eue aucune influence sur la compréhension de l'évolution naturelle ou de la relativité générale.
Cette bataille n'a eu aucune influence sur la façon dont mes ancêtres voyaient le monde.
Et donc, elle n'a eu aucune influence sur la façon dont je vois le monde.
La majorité des autres batailles est similaire.
Elles ont eu une influence sur la vie de certaines personnes, mais simplement au niveau organisationnel.

Par exemple, cette bataille n'a même pas influencée le livre «~l'art de la guerre~» de \propre[y]{Nicolas}{Machiavel} écrit en \Annee{1519}-\Annee{1520}.
Ce livre que certains considèrent comme un chef d'oeuvre.
Certains voudraient, le mettre au rang d'un \propre[y]{Sun}{Tzu}, d'un \propre[y]{Antoine-Henri}{Jomini} ou \propre[y]{Carl von}{Clausewitz}.
Son refus de considérer l'artillerie de façon sérieuse, parce que les militaires ne la maîtrisaient pas, n'est qu'un des nombreux reproches qui me font considérer son livre comme étant nul.
La bataille de \Index{Marignan} était la première bataille dans laquelle l'artillerie s'est montrée décisive.
Elle aurait dû lui faire reconsidérer sa position.
Comme il était le seul à l'époque (\propre[y]{Sun}{Tzu} n'était pas encore connu en Europe) à se prendre pour un stratège. 
Cette bataille aurait pu avoir influencé la vision de la stratégie de mes ancêtres.
Ça n'a pas été le cas. 

Je ne vais pas collectionner les dates comme des pierres précieuses.
Pour moi, la terre a été créée il y a 4,5 milliards d'années.
Si des calculs montrent que cette date est fausse et qu'il faut rajeunir ou vieillir l'âge de la terre d'un demi milliard d'années, ça ne m'intéressera pas vraiment.
Pour des raisons de précision, je prendrai probablement en compte cette nouvelle estimation.
Mais ça ne changera rien à l'évolution globale, ça ne remettra absolument pas en cause tout ce que j'ai lu jusqu'à maintenant.

Si vous avez bien compris les citations qui débutent ce texte, vous avez dû comprendre plusieurs choses.
Ce qui suit est d'abord pour moi.
La seule façon de savoir si je suis capable d'expliquer l'histoire de mes ancêtres à ma grand-mère est d'en écrire le bilan.
Comme mes deux grands-mères sont encore vivantes, je n'ai pas voulu choisir entre les deux.
Il faut que je sois capable d'expliquer mon bilan à mes deux grands-mères. 

Ensuite, ce ne sera probablement jamais fini.
En lisant d'autres livres, je peux apprendre des nouveautés qui remettent en question ce que je croyais acquis.
En parlant avec d'autres personnes aussi.
La science faisant continuellement des progrès, j'aurai probablement toujours des corrections à apporter.
Il y aura toujours des points noirs, mais ce ne sera pas de ma faute, ce sera de la faute des scientifiques qui n'auront pas répondus à toutes les questions.
Ou plutôt, ce n'est pas vraiment de la faute des scientifiques s'ils ne savent pas répondre à toutes les questions, c'est de la faute du monde qui est trop compliqué pour être entièrement compris.

Les façons de se considérer par rapport aux autres ont évolué.
Ce qui va m'intéresser ici, ce sera de savoir comment j'en suis arrivé là.
Comment mes ancêtres voyaient les choses et qu'est ce qui a fait changer leur vision des choses.
Que m'ont légué mes ancêtres dans ma façon de voir le monde ?

Je ne suis pas la meilleur source pour vous aider, mais si vous y tenez, vous pouvez vous appuyer sur ces pages.
Vous apprendrez peut-être des choses qui vous concernent.
Si vous n'êtes pas Français (personne n'est parfait), vous apprendrez peut-être des choses à propos de gens que vous connaissez.
Si vous êtes Français, vous apprendrez peut-être des choses sur vos ancêtres.
Mais ma vision de voir est subjective, je vous conseille de vérifier ce que vous pourriez apprendre sur ces pages.

Quelques soient vos connaissances, ces pages ne sont pas suffisantes en elles-mêmes.
Elles sont un résumé et une réflexion (je sais, mes réflexions sont limitées, mais elles ont le mérite de me faire avancer) sur ce que j'ai pu lire à divers endroits.
Pour connaître vos ancêtres, vous avez forcément besoin de plusieurs sources.
Cette source n'a ni la prétention d'être exhaustive, ni la prétention d'être objective.
Cette source n'a même pas la prétention de vous être utile.
Si vos connaissances sont limitées, vous avez besoin d'autres sources pour confirmer ou infirmer ce que vous pourriez lire ici.
Si vos connaissances sont évoluées, vous n'allez rien apprendre ici.
Je ne comprends même pas pourquoi vous continuez à me lire.
Comme je l'ai écrit, elle a simplement la prétention de m'être utile.

C'est un livre qui me prends beaucoup de temps et qui évolue.
J'ai dû le commencer vers \Annee{2009}-\Annee{2010}.
Il a beaucoup évolué, il y a des choses que j'ai écrites au début et qui ne sont peut-être plus vraies maintenant.
Voire, qui sont en contradictions avec d'autres parties (mais j'essaye de l'éviter le plus possible).
Je ne peux pas à la fois corriger les anciennes parties et en ajouter d'autres.
J'essaye d'avoir un livre homogène, mais ce n'est pas toujours possible.

Je vais écrire comme si je m'adressais à quelqu'un, c'est plus simple pour la rédaction.
Si je ne suis pas capable d'expliquer un point, c'est que je ne l'ai pas compris. 
Si je ne l'ai pas compris, cela veut dire que je dois approfondir mes recherches dans le domaine.

Je ne vais pas me cacher derrière un «~nous~».
Le «~nous~» a deux effets que je n'aime pas.
Le premier, c'est qu'il donne au lecteur l'impression que tout le monde est d'accord avec ce qui est écrit.
Le second, c'est qu'il force le lecteur à avoir l'impression d'être implicitement d'accord avec ce qui est écrit.
J'assume ce que j'écris, je sais que j'ai raison, mais j'autorise un éventuel lecteur à ne pas être d'accord avec moi, je vais utiliser le «~je~».
