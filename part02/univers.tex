%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
%  Fichier : univers.tex
%    Modif : ven. 11 oct. 2013 15:11
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Chapitre{Que sait-on de l'univers ?}

\citation{\propre{Bill}{Bryson}}{The upshot of all this is that we live in a universe whose age we can't quite compute, surrounded by stars whose distances we don't altogether know, filled with matter we can't identify, operating in conformance with physical laws whose properties we don’t truly understand.}{Le résultat de tout ça est que nous vivons dans un univers dont nous ne pouvons pas vraiment calculer l'âge, entouré d'étoiles dont nous ne connaissons pas entièrement les distances, rempli de matière que nous ne pouvons identifier, opérant en conformité avec des lois physiques dont nous ne comprenons pas réellement les propriétés.}

\section{Les observations}
En regardant le ciel, l'homme peut faire des observations.
C'est ce qu'il a fait depuis la nuit des temps.
Mais l'œil est un peu limité, il ne peut voir que \Nombre{5000} étoiles et il ne peut voir qu'une partie du spectre lumineux.
Or, il y a des ondes en dehors de la lumière visible qui donnent beaucoup d'informations.
C'est l'intérêt des télescopes et de certains satellites.
De voir plus loin que l'œil et de détecter des ondes que l'œil ne voit pas.

Puisque nous savons que les galaxies s'éloignent et que nous savons où elles sont, il est possible de calculer leurs trajectoires pour savoir d'où elles viennent.
En appliquant la relativité générale à ce que nous voyons du ciel, nous pouvons voir que toutes les galaxies que nous observons viennent d'un espace très restreint.
C'est ce qui est appelé le Big bang, il a eu lieu il y a \Nombre{13,7} milliards d'années.
C'est la naissance de ce que nous connaissons de l'univers.
Je vais revenir sur ce que j'ai écrit plus haut pour préciser les choses.

Supposons que vous entendiez une voiture puissante passer rapidement à côté de vous.
D'abord, vous entendez un son de plus en plus aigu lorsque la voiture se rapproche de vous.
Ensuite, le son est de plus en plus grave lorsqu'elle s'éloigne, c'est l'effet Doppler.
L'onde émise par le son de la voiture change avec le déplacement de la voiture.
La lumière est aussi une onde.
Depuis \propre[y]{Albert}{Einstein}, nous savons que la vitesse de la lumière n'est pas infinie, mais est d'environ \Unite{300 000}{km/s}.
En fait, cette connaissance est plus ancienne, mais il l'a utilisée pour poser les bases de sa science.
Ce qui est important, pour la relativité, c'est que la vitesse de la lumière est constante dans le vide.
L'effet Doppler s'applique donc aussi à la lumière.
Comme la lumière émise par les galaxies lointaines est décalée dans le rouge, c'est que ces galaxies s'éloignent de nous.

Cependant, le décalage est plus important que ce que cet effet tout seul pourrait expliquer.
En fait, les galaxies s'éloignent les unes des autres de plus en plus vite.
Elles s'éloignent tellement vite que celles qui sont le plus loin s'éloignent plus vite que la vitesse de la lumière.
Si rien ne peut se déplacer plus vite que la lumière dans l'espace, cette contrainte ne s'applique pas à l'espace lui même.
Une galaxie qui est très loin de nous, nous envoie de la lumière.
Cependant, comme nous nous éloignons de cette galaxie plus vite que la lumière qu'elle nous envoie, cette lumière ne pourra jamais nous atteindre.
Donc, la lumière issue des galaxies les plus loin qui rayonnent aujourd'hui ne nous parviendra jamais.
La lumière des galaxies les plus éloignées que nous voyons actuellement a été émise avant que ces galaxies ne s'éloignent de nous plus vite que la lumière.
Dans 100 milliards d'années, seules les galaxies les plus proches de nous seront visibles.

Nous nous trouvons dans un univers observable de 46 milliards d'années-lumière.
Tout ce qui est hors de cette distance est inexistant pour nous.
Nous somme donc au centre de l'univers.
S'il existe de la vie dans une autre galaxie, les êtres vivants sont aussi au centre d'un univers de 46 milliards d'années-lumière.
C'est un autre univers qui peut avoir des galaxies en commun avec le notre (cela dépend de la distance de cette galaxie).

Quand j'écrivais que toutes les galaxies que nous observons viennent d'un espace très restreint, c'est de cet univers observable dont il était question.
Le bigbang s'est produit partout en même temps.

Comme le soleil est très loin de nous, la lumière qu'il nous envoie met 8 minutes pour nous arriver.
Cela veut dire que nous ne voyons pas le soleil, mais nous voyons le soleil tel qu'il était il y a 8 minutes.
Le soleil met 24 heures à faire le tour de la terre (vous pouvez considérer que c'est la terre qui tourne sur elle même en 24 heures, ça revient au même).
Donc, il parcourt 360 degrés en 24 heures, ce qui fait 2 degrés en 8 minutes.
C'est très faible, et vous pouvez considérer que vous voyez le soleil.
Cependant, l'étoile la plus proche de la terre (en dehors du soleil), proxima du centaure, est à un peu plus de 4 années lumières de nous.
Cela veut dire que nous voyons cette étoile telle qu'elle était il y a un peu plus de 4 ans.

\section{Le passé de l'univers}

Comme c'est l'étoile la plus proche de nous, ce que nous voyons des autres étoiles est encore plus ancien.
Et donc, plus nous regardons loin, plus nous voyons dans le passé.
Si nous voyons assez loin, nous pourront donc voir la création de l'univers ?
Malheureusement, non.
Nous voyons les étoiles parce qu'elles émettent de la lumière.
Nous voyons la lune et les planètes du système solaire parce qu'elles sont suffisamment proches pour refléter la lumière du soleil.
Mais nous ne voyons pas les autres planètes de l'univers.
Pour l'univers, c'est la même chose.
Nous ne pouvons pas voir ce qu'il y avait avant qu'il émette de la lumière.
Le plus ancien que l'on puisse voir est \Nombre{380 000} ans après sa création.
Ce n'est pas de la lumière, car la longueur d'onde émise à l'époque a changé et est maintenant une onde radio.

Vous pouvez réussir à prendre la photo d'un ovule juste après sa fécondation par un spermatozoïde.
Cette photo peut vous donner des renseignements intéressants.
Cependant, à partir de cette photo, vous ne pouvez pas prédire l'apparence du foetus, du nouveau né ou de l'adulte.
Par contre, si vous connaissez la date et l'heure de cette photo, vous pourrez savoir quand le foetus naîtra de façon assez précise.
Pour l'univers, le principe est le même.
Cette image de l'univers ne peut pas nous permettre de prévoir ce qu'il y avait avant ou ce qu'il y aura après.
Mais elle peut nous aider à valider ou infirmer certaines théories.

Cette image d'un univers homogène au bout de \Nombre{380 000} ans est compatible avec la théorie du big bang.
Grâce à la relativité générale, il est possible de remonter plus loin dans le passé.
Les scientifiques remontent très loin, mais pas au début.
Il pensent savoir ce qu'il s'est passé \Nombre{1e-43} secondes après le bigbang.
C'est très court, c'est une fraction de seconde tellement petite que c'est inimaginable, c'est plus court que tout ce que vous connaissez.
C'est un nombre avec 43 chiffres après la virgule : 0,000000...000001 secondes.
Il y a 42 zéros avant le un.
Avant \Nombre{1e-43} secondes, les équations de la relativité générale sont inutilisables.
Ce nombre, c'est la constante de \propre[y]{x}{Plank} et elle est très utilisée en physique quantique.
Personne ne sait ce qu'il s'est passé pendant ce temps très court.

\section{Sa naissance}

Il y a beaucoup de théories, les scientifiques ont beaucoup d'imagination.
Mais il n'y a aucune certitude.
L'idée partagée par la majorité des scientifiques est le temps et les dimensions sont créées au moment du Big Bang.

Cela est très compliqué à concevoir.
Cette conception du Big Bang ne veut pas dire qu'avant il n'y avait rien, puis après il y a eu quelque chose.
Cela voudrait dire qu'il serait possible de sortir de l'univers et du temps pour le regarder naître.
Mais ce n'est pas possible.
L'univers, c'est la totalité de ce qui existe, il n'y a rien en dehors de l'univers.
Si vous voulez mettre l'univers dans quelque chose, vous ne faites que repousser le problème : ce quelque chose, dans quoi pouvez vous le mettre ?
Cette conception de l'univers veut dire que le Big Bang est le début du temps.

Vous ne pouvez pas vous mettre en dehors du temps pour le regarder commencer.
Ce serait chercher à vous mettre au nord du pôle nord ou au milieu du centre d'un cercle.
Il n'y a pas de nord du pôle nord, comme il n'y a pas de milieu du centre d'un cercle.

Pour essayer de le comprendre, commençons par la lumière.
Nous avons des notions d'ombre et de lumière.
Quand il fait noir, c'est l'absence de lumière.
Le noir n'existe pas en lui même, nous arrivons bien à le concevoir.

Prenons autre chose par exemple, qui est plus dur à concevoir, la chaleur.
Nous avons des notions de chaud et de froid avec des températures positives et négatives.
En fait, ce n'est qu'une conception.
La chaleur existe, pas le froid.

Le froid est l'absence de chaleur, nous avons du mal à le concevoir parce que nous n'avons jamais connu le froid total.
Mais d'un autre côté, nous pouvons le concevoir lorsque la température n'est pas bonne.
Lorsque nous avons froid, nous avons des moyens de nous réchauffer, comme bouger, se couvrir ou faire un feu par exemple.
Par contre, lorsque nous avons trop chaud, nous ne savons pas faire du froid.
Nous savons enlever la chaleur d'un endroit pour la mettre autre part (comme un réfrigérateur ou une climatisation).
C'est comme pour la lumière, nous savons empêcher la lumière de venir, mais nous ne savons pas créer de noir.

Le froid absolu, c'est \Unite{-273,15}{\degreeCelsius} ou \Unite{0}{\degree}Kelvin.
A cette température, il n'y a pas de chaleur, c'est à dire que les électrons sont immobiles autour des atomes, cette température n'est donc pas atteignable.
C'est l'absence absolue de chaleur.
C'est le vrai froid théorique, il n'y a pas plus froid.

Pour cette vision du Big Bang, c'est le même principe.
Il n'y a pas d'avant le début, il n'y a pas d'en dehors de l'univers.

Il y a une autre vision du Big Bang que je vais indiquer ici pour des raisons d'honnêteté intellectuelle.
Il y a beaucoup de chercheurs qui sont d'accord avec cette autre vision, je n'ai pas la prétention d'être plus intelligent ou de mieux connaître le sujet qu'eux.
Cependant, il n'y a aucun moyen d'infirmer ou de valider cette autre hypothèse.
Je considère que cette vision est plutôt une façon de déplacer les problèmes de considération de l'univers.
C'est la première raison pour laquelle je n'aime pas cette vision.

L'autre vision est qu'avant le bigbang, il y avait un autre univers.
Cet autre univers s'est contracté sous l'effet de la gravitation, il se serait tellement contracté que ses propriétés se seraient modifiées.
Cette contraction s'appelle le Big Crunch.
Puis il aurait explosé pour recréer un bigbang.
Cette vision reviendrait à dire qu'avant nous, il y aurait eu une infinité de bigbangs.
Chaque bigbang aurait créé un univers qui se serait accru, puis se serait contracté sous la force de la gravité, puis aurait donné un Big Crunch.
Le Big Crunch donnant naissance à un autre bigbang.

\section{Intermède gravitationnel}

Comme les propriétés de l'univers précédant ont été modifiées par le passage du Big Crunch au Big Bang, il n'est pas possible de connaître les propriété de l'univers précédant.
Par propriétés, j'entends les propriétés fondamentales comme la valeur de la force de gravitation.
Puisque je parle de la gravitation, et que je vais continuer à en parler, je vais expliquer ce que c'est maintenant.

Dans l'esprit des gens, la gravitation est conforme à la vision d'\propre[y]{Isaac}{Newton}.
Les gens voient la gravitation, comme une force qui fait que deux corps s'attirent, un peu comme un aimant et du fer s'attirent.
D'après \propre[y]{Albert}{Einstein}, et sa relativité générale, si les corps s'attirent, c'est parce que les corps déforment l'espace qui les entourent.

Prenons une image pour simplifier.
Si vous avez un lit bien fait, plat sans le moindre pli.
Au milieu de ce lit, vous posez un gros livre.
Il s'enfonce dans le lit.
Si vous mettez une bille sur ce lit loin du livre, cette bille ne bouge pas.
Si vous rapprochez cette bille du livre, elle va se déplacer toutes seule vers le livre.
Vous savez très bien que le livre n'a pas attiré la bille.
Vous savez que le livre a déformé le lit.
Vous savez que c'est cette déformation qui a fait tomber la bille.
Cependant, vous avez l'impression que le livre a attiré la bille.

Pour la gravitation, l'effet est plus dur à concevoir, mais le principe est le même.
La difficulté, c'est que la surface du lit est en deux dimensions.
Comme nous sommes en trois dimensions, nous voyons la troisième dimension sur laquelle s'applique cette déformation.
Alors que la gravité s'applique sur une quatrième dimension, que nous ne pouvons donc pas voir.

Une autre image pour pouvoir comprendre la gravité est de regarder la terre.
Vue de l'espace, la terre est ronde.
Vu de chez moi, elle semble plate.
La raison, c'est que la rotondité de la terre est très faible.
Si vous allez près du pôle nord avec quelqu'un.
Vous avez tous les deux un compas super précis qui indique le nord sans la moindre possibilité de se tromper.
C'est une supposition, aux alentours du pôle nord, seul un GPS peut vous renseigner, mais c'est pour comprendre.
En allant vers le pôle nord, vous aurez l'impression d'aller en ligne droite sur du plat.
En vous déplaçant, vous aurez l'impression que l'autre personne se rapproche de vous au lieu de marcher parallèlement à vous.
Vous pouvez aussi avoir l'impression qu'une force vous attire l'un à l'autre.

La vision d'\propre[y]{Isaac}{Newton}, si elle est imprecise dans le détail, est quand même suffisamment précise pour être généralement utilisée.
Les calculs d'\propre[y]{Albert}{Einstein} sont beaucoup plus compliqués que ceux d'\propre[y]{Isaac}{Newton} pour un écart très faible.
Il pourra m'arriver d'utiliser le terme de gravité pour des raisons de simplicité.
Mais il faut bien comprendre que cette force n'existe pas, c'est un abus de langage qui exprime une idée communément comprise.

\section{Pour en revenir au Big Crunch}

La seconde raison qui fait que je ne considère pas cette vision comme valide, c'est que nous serions dans le dernier univers.
D'après les lois de l'univers actuel, j'ai écrit plus haut que les galaxies s'éloignent tellement vite qu'elles seront hors de notre vision.

D'après les équations de la relativité appliquées à l'univers observable, l'univers devrait soit s'étendre, soit se contracter.
Le principe est simple.
Lancez un cailloux en l'air (en faisant attention qu'il n'y ait personne devant vous).
Vous savez ce qu'il se passe, le cailloux monte de moins en moins vite, puis retombe.
Le principe est simple, la vitesse initiale à laquelle vous lancez le cailloux le fait monter.
La gravité l'attire vers la terre, ce qui le ralentit puis le fait retomber, en accélérant, vers la terre.
Si vous lancez le cailloux beaucoup plus fort (n'essayez pas, vous n'y arriverez pas, c'est une image), votre cailloux va aller dans l'espace.
Pour ça, il faut que la vitesse initiale de votre cailloux soit suffisamment élevée pour que la gravitation ne le ralentisse pas assez.
Une fois que le cailloux a quitté la gravitation terrestre, il continuera à avancer à la même vitesse dans la même direction.
Si vous le lancez à une vitesse intermédiaire, il sera en orbite autour de la terre, sa vitesse compensant exactement la gravitation.

Pour l'espace, le problème est le même.
La vitesse initiale de son expansion aurait pu être faible.
La gravitation aurait été plus forte et au bout d'un moment, touts les éléments se seraient attirés par gravitation et il se serait replié sur lui même.
La vitesse initiale aurait pu être très forte et au bout d'un moment, les éléments se seraient assez éloignés pour ne plus être attirés entre eux par leur gravitation.
Il aurait continué à s'étendre à la même vitesse.
D'après les observations, nous sommes dans le second cas.
L'univers ne se repliera jamais sur lui même et s'il y a eu une infinité de Big Bang suivis de Big Crunch, la série s'arrêtera avec notre univers.

En théorie du moins, car il y a quand même beaucoup d'inconnues.
La première inconnue, c'est que la gravité des galaxies n'est pas correctement expliquée par la relativité générale.
Pour que la relativité générale puisse expliquer la gravité entre les galaxies, il faudrait qu'il y ait beaucoup plus de matière.
L'existence des trous noirs et des planètes que nous ne voyons pas ne suffisent pas à expliquer cet écart.
Il est donc supposé qu'il y ait de la matière noire dans l'univers.
Personne ne sait rien de cette matière, sauf qu'elle agit sur la gravitation.
Il y aurait cinq fois plus de matière noire que de matière appelée matière baryonique par les scientifique.
La matière baryonique regroupe toute la matière composée d'atomes (planètes, soleils, trous noirs et autres comètes), en gros ce que nous pouvons voir.

Et ça, ce n'est que la première incertitude.
Une fois que la matière noire explique la gravité des galaxies, il reste à expliquer l'expansion de l'univers.
Comme je l'ai écrit plus haut, l'univers s'étend de plus en plus vite.
Et ce n'est pas explicable.
Pour pouvoir expliquer cette accélération de son expansion, il faut supposer de l'énergie qui agirait de façon inverse à la gravité.
Cette énergie, appelée énergie noire, serait aussi en très grosse quantité.
Bien sûr, personne ne sait rien de cette énergie noire.
C'est cette énergie noire qui empêcherait un Big Crunch.
Il y aurait trois fois plus d'énergie noire que de matière.

Là, certains peuvent ce demander à quoi correspond une correspondance entre une quantité de matière et une quantité d'énergie.
Dans notre vision du monde, l'énergie et la matière sont totalement distinctes.
Séparer quelque chose en mettant d'un côté l'énergie et de l'autre la matière est dur à concevoir.
Pourtant, c'est ce que fait la formule scientifique la plus célèbre du monde.
Tout le monde sait qu'Einstein a dit : «~$E=mc^{2}$~» même si tout le monde ne sait pas à quoi cette formule correspond.
Le «~$E$~» correspond à une quantité d'énergie.
Le «~$m$~» est la masse de la matière.
Le «~$c^{2}$~» est une constante, c'est la vitesse de la lumière au carré qui correspond à \Nombre{9e10}, c'est à dire à un 9 suivi de 10 zéro (j'arrondis, la valeur exacte est un peu plus faible).

Cette formule à deux intérêts.
D'abord, elle montre une relation directe entre l'énergie et la matière.
Je veux dire par là que la matière peut être transformée en énergie.
L'énergie peut aussi être transformée en matière.

La seconde indication donnée par cette formule est qu'un peu de matière peut donner énormément d'énergie.
Le problème, qui est aussi un garde-fou, c'est que nous n'arrivons pas encore à utiliser une grosse partie de cette énergie disponible.
Quand nous voyons les dégâts faits par une bombe atomique ou par un accidents dans une centrale nucléaire, nous voyons pourquoi c'est un garde-fou.
Par contre, si nous étions capable de mieux convertir la matière en énergie, il n'y aurait plus de pénurie de pétrole ou de pollution due à la production d'énergie (pour notre niveau de vie actuel j'entends, avec l'avancée scientifique, nous pourrons avoir de nouveau besoins énergivores).

\section{Ce que nous croyons savoir}

Donc, pour résumer la composition de l'univers, il y a en gros \Unite{4}{\percent} de matière baryonique, \Unite{21}{\percent} de matière noire et \Unite{75}{\percent} d'énergie noire.
Pour voir les choses d'une autre façon, il y a \Unite{4}{\percent} de l'univers sur lequel des hypothèses peuvent être faites à partir de ce que nous voyons et \Unite{96}{\percent} de l'univers dont personne ne sait rien mais qui est utile pour expliquer ce que nous voyons.
Il y a des supposition, mais rien de vraiment avancé.
La science a encore de gros progrès à faire dans la connaissance de l'univers.
Ce qui est écrit ci dessus va être invalidé avec l'avancée de la science (enfin, en partie, tout n'est pas forcément à jeter).

J'espère que les invalidations de ce que j'ai écrit seront tardives.
Je n'ai pas envie de tout réécrire.

Donc, revenons en aux faits.
Il y a \Nombre{13,7} milliards d'années, c'était le Big Bang.
Le temps commence, les dimensions existent, la matière se créée.
Ou plutôt, une partie de la matière.
Très vite, il y a de l'hydrogène et des étoiles.

Il y a \Nombre{4,5} milliards d'années, une étoile meurt.
Pas n'importe quelle étoile, une étoile très dense qui meurt violemment en explosant.
Pendant sa vie, cette étoile a créé de la matière, cette explosion s'appelle une \Index{supernova}.
Au centre, un trou noir est créé, à l'extérieur, la matière est éjectée.
Cette \Index{supernova}, avec l'énergie produite et la matière libérée, a permis au système solaire de se créer.

La matière se rassemble en fonction de la gravité pour former le soleil avec des anneaux.
Les anneaux se regroupent pour former les planètes.
Parmi les planète, la terre est créée.

Puis, un astéroïde vient heurter brutalement la terre.
Il est absorbé par le choc, sous la violence de la collision, une partie du manteau terrestre se sépare de la terre pour créer la lune.
Ce qui se sépare de la terre est de la poussière ou des cailloux.
Une partie de cette poussière retombe sur terre avec la force de gravité, l'autre partie reste en orbite.
Sous l'effet combiné de la gravité et de la rotation autour de la terre, cette poussière se regroupe.
Cela forme la lune.

La lune, en orbite autour de la terre, fait ralentir la vitesse de rotation terrestre (les jours deviennent plus longs).
Elle oblige aussi l'axe autour duquel la terre tourne à rester à peu près stable.
Cela permet de limiter les variations climatiques.
Elle permet aussi d'aider les marées, mais cette influence est plus récente (au début, il n'y avait pas d'océan sur terre, donc pas de marée).
La lune est donc vitale à plus d'un titre à la vie sur terre telle que nous la connaissons.
Maintenant que la terre et la lune sont crées, la terre va pouvoir évoluer.

Je vous avais prévenu.
Je n'ai pas réussi à rire en parlant du Big Bang.
Pas le moindre jeu de mot ou la moindre blague pendant tout un chapitre.
C'est triste.
Pour égailler un peu ce chapitre, je vous serais reconnaissant de bien vouloir rire.


