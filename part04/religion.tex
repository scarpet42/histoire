%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : religion.tex
% 	Modif : ven. 11 oct. 2013 15:44
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Chapitre{La religion}

\citation{\propre{Albert}{Einstein}}{Science not only purifies the religious impulse of the dross of its anthropomorphism but also contributes to a religious spiritualization of our understanding of life.}{La science non seulement purifie l'impulsion religieuse des ordures de son anthropomorphisme mais contribue aussi à une spiritualisation religieuse de notre compréhension de la nature.}

\citation{\propre{Henri}{Poincaré}}{Douter de tout ou tout croire, ce sont deux solutions également commodes, qui l'une et l'autre nous dispensent de réfléchir.}{x}

\citation{\propre{Walter}{Lippmann}}{Where all think alike, no one thinks very much.}{Quand tous pensent pareil, personne ne pense beaucoup.}

\section{Avant propos}

Comme je l'ai dit au début, je ne vais pas essayer de démontrer que Dieu existe ou pas, ce n'est, à l'heure de la science actuelle, pas possible.
Je peux encore moins démontrer quelle est la religion qui se rapproche le plus de Dieu s'il existe.
Par contre, sous prétexte que je n'ai rien à apporter sur l'existence ou la non existence de Dieu, je ne peux pas taire la religion.
Je parle ici de l'histoire de mes ancêtres, de la façon dont ils voyaient le monde.
Il est indéniable que des concepts très importants comme «~les droits de l'homme~» reposent, au moins en partie, sur la religion chrétienne.
Qui elle a des racines beaucoup plus anciennes, la plus récente de ses racines étant la religion hébraïque.
Je suis donc obligé d'étudier l'histoire de la religion, c'est à dire l'histoire dont les hommes ont considéré leur rapport avec la nature au fil du temps.

Aujourd'hui, en \propre[n]{x}{France} (ainsi que dans tous les pays industrialisés dont j'ai pu lire des auteurs), il n'y a que deux façons de concevoir la vie et Dieu.
La première, est qu'il existe une (ou plusieurs entités supérieures) qui a (ou qui ont) créé l'homme (de manière plus ou moins directe en fonction des conceptions).
Dans cette vision, l'homme est composé de deux parties.
Une partie physique, qui est son corps, et une partie spirituelle, qui est son âme.
Il y a donc une vie après la mort.
La seconde, à la fois la plus ancienne et la plus récente, est que Dieu n'existe pas (ni aucune entité supérieure équivalente).
La plus ancienne, parce qu'avant que l'homme n'ait conscience de son état, il ne croyait rien.
C'est quand l'homme commence à enterrer ses morts que certains historiens considèrent que l'homme devient humain.
La plus récente, car c'est avec l'évolution de la science et le relevés d'incompatibilités avec la Bible que l'existence de Dieu a commencée à être mise en cause.
Dans cette vision, l'homme a été créé de la façon détaillée au dessus, je ne vais pas y revenir.
Mais il y a un point important, c'est que seule sa partie physique a été créée, il n'a pas d'âme.
Il n'y a donc pas de vie après la mort.
Pour résumer, il n'y a que deux visions.
Soit Dieu et une vie après la mort, soit pas de Dieu et une mort définitive.
Ou, dit de manière différente, soit un Dieu (ou plusieurs) et une existence à la fois spirituelle et matérielle.
Soit aucun dieu et une existence entièrement matérielle.

C'est quelque chose qui me choque, car quand un processus important s'est mis en place inconsciemment, au moment de sa remise en cause, toutes les hypothèses devraient être étudiées.
Pour moi, deux hypothèses ne sont jamais prises en compte (en tous cas, dans ce que j'ai lu).
C'est un manque.
J'ai l'impression d'être seul au monde.
C'est triste.
Je vais donc donner ces deux visions ici, rapidement, par souci de complétude, mais je ne les étudierai pas puisque mes ancêtres ne s'y sont pas attardés.
Le premier manque est que si Dieu n'existe pas, la vie après la mort est quand même envisageable.
Si la vie est apparue d'elle même, je ne vois rien qui aurait empêché les êtres vivants d'être créés avec une âme ou quelque chose équivalent.
Vous devez deviner le second manque, c'est que si Dieu existe, je ne vois pas pourquoi il aurait forcément créé une vie éternelle pour l'homme.
J'ai deux raisons pour aller dans ce sens.
D'abord, c'est que si Dieu a été assez puissant pour créer l'univers, il peut le considérer dans son ensemble et mépriser les créatures individuelles.
Ensuite, si Dieu s'intéresse à ses créatures individuellement, il n'est pas obligé pour autant de vouloir leur donner la vie éternelle.
La vie, telle qu'elle a été créée (ou plutôt telle que la science actuelle la connait), est basée sur l'évolution et donc sur la mort.
Une vie éternelle, et donc une âme, pourrait aller dans le sens contraire du choix de Dieu.

Je voudrais revenir sur le fait que je suis seul au monde à considérer les deux autres possibilités.
Il m'arrive d'entendre ou de lire que les hommes ont cru en Dieu parce qu'ils avaient peur de la mort.
C'est faux, \propre[y]{Jules}{César} nous l'a montré dans la guerre des \propre[n]{x}{Gaules}.
Un passage rarement cité est très important.
Il nous dit que les Gaulois n'avaient pas peur de mourir parce qu'ils avaient inventé la vie après la mort.
Vous trouvez que ce passage va dans le sens que je voudrais réfuter ?
Vu par les gaulois, vous auriez raison, mais ce qui m'intéresse ici est la vision de \propre[y]{Jules}{César}, et donc des Romains.
Ici, en appuyant la vision des Gaulois, il nous montre que les Romains ne croyaient pas dans une vie après la mort.
Il nous montre aussi que les Grecs n'y croyaient pas non plus.
Il connaissait bien la religion grecque, en dehors de la fondation de Rome, c'est elle qui a tout donné à la religion romaine.
Pour les Grecs, l'existence du \Index{Styx} semble aller dans le sens inverse.
Mais il est tout à fait possible que la conception du royaume des mort était une vision floue qui ne concernait pas directement leur propre mort.
Si la vision d'un monde avec dieux et sans immortalité de l'âme n'existe plus aujourd'hui, elle a donc pu exister il y a longtemps et je me sens un peu moins seul.

\section{Avant la religion}

Si l'église catholique a si longtemps voulu imposer sa vision à la science, c'est qu'elle a les mêmes origines.
La religion, comme la science, est née de la superstition.
Les premiers hommes ont vu que la nature était beaucoup plus puissante qu'eux et que son comportement n'était pas entièrement aléatoire.
La superstition a évolué vers la science pour la compréhension de ce qui l'entoure et vers la religion pour la compréhension du comportement attendu par la nature.
C'est flou, j'en suis conscient, mais l'origine l'était aussi.
C'est facile avec nos yeux d'occidentaux du \siecle{xxi} siècle de faire la part entre la science et la religion.
Mais lorsque les premiers \Index{chamane}s (je n'ai pas de mot plus approprié, je n'utiliserai pas le terme sorcier qui est trop éloigné du contexte dans certaines de ses acceptations) faisaient trois fois le tour du malade en lui soufflant de la fumée dessus et en lui faisant boire une potion, ils ne savaient pas ce qui était important et ce qui ne l'était pas.
Le rituel faisait un tout.
Mi-religieux, mi-scientifique.
Il ne savait pas si leur rituel agissait directement ou si le rituel permettait à une entité supérieure d'intervenir.

Il n'est pas possible de demander aux premiers hommes (ou à leurs ancêtres) comment ils vivaient, mais il est possible de réfléchir.
Une aide à la réflexion repose sur l'étude des chasseurs cueilleurs dans le monde (actuel et passé avec les témoignages des explorateurs).
Lorsqu'une vision est répandue entre des peuples qui ne se sont jamais rencontrés, il est possible d'imaginer qu'elle a des racines profondes liées au mode de vie.
Les premiers hommes, comme leurs ancêtres, observaient la nature.
C'est une évidence, leur faible capacité physique (pas rapport aux autres animaux, pas par rapport à moi. Quoique\ldots) et leur besoin de manger de la viande (ou plutôt des protéines, mais c'est assez lié) l'exigeait.
Sans avoir observé la nature, ils auraient disparu et je ne serais pas là pour me demander ce qu'ils pouvaient penser.
Et vous ne pourriez même pas être là pour non plus pour vous demander pourquoi vous continuez à me lire.
Une observation simpliste permet de remarquer que la nature forme un tout.
Lorsqu'il y a du vent, les nuages se déplacent.
Les nuages apportent la pluie, qui à son tour abreuve les plantes et remplit les lacs et rivières.
Les hommes peuvent ainsi boire.
L'alternance entre la nuit et le jour, les différentes saisons, et leurs implications est rapide à réaliser.
Par exemple, certains animaux ne se voient pas en hiver.
Les feuilles tombent des arbres en hivers, les fleurs poussent au printemps.

Dans le chapitre sur l'histoire de la science, j'expliquais que l'évolution des idées ne progressait que lorsqu'un problème se présentait.
Le paragraphe précédent peut sembler remettre cette explication en cause.
C'est juste parce que je simplifie mes phrases.
Mais l'homme qui voit la pluie tomber peut se demander pourquoi elle s'arrête.
L'homme qui observe un ciel bleu peut se demander pourquoi les nuages apparaissent.
L'homme qui observe des arbres feuillus peut se demander pourquoi les feuilles tombes.
Bref, il remarque un changement, ne comprend pas pourquoi et cherche une explication.
Par exemple, il ne se demande pas pourquoi il y a une pierre sur la route.
La pierre a toujours été là.
Sauf s'il l'a déplacée lui-même ou s'il a vu quelqu'un la déplacer.

Toutes ces remarquent en apportent une plus globale : toute la nature semble aller de concert.
Cela apporte deux questions~:
\begin{tuxliste}
\item Comment la nature se comporte ?  C'est la science qui devra y répondre.
\item Comment l'homme doit se comporter pour être en harmonie avec la nature ? C'est la religion qui devra y répondre.
\end{tuxliste}
S'il est clair que ces questions sont liées, il a fallu beaucoup de temps pour les dissocier.
Les premiers hommes avaient donc conscience d'une nature unie.
Cette nature unie était beaucoup plus puissante qu'eux, il était rapide de la déifier.
C'est elle qui donnait la vie et qui pouvait la reprendre.
Et qui la reprenait souvent d'ailleurs.
Elle devait être vue comme une déesse.
C'est probablement elle qui a donné \Index{Birgidh} et \Index{Gaïa} dans les mythologies celtes et grecques.
 
\section{Le polythéisme}

Le passage de la religion d'une nature globale un ensemble de dieux a dû se faire en douceur.
C'est une avancée graduelle dans la conception d'une entité supérieure.
L'homme réalise que des événements naturels sont totalement indépendants.
Il doit même finir par réaliser que des faits naturels ne sont pas toujours entièrement en harmonie.
Par exemple, la foudre qui tombe sur un arbre et fait brûler une forêt doit amener à se poser des questions.
Il devient possible de se demander si, à la place d'une nature unie, il n'y aurait pas plusieurs entités supérieures.
Des entités qui pourraient vivre en harmonie entre elles, tout en ayant quelques points de friction.

Une question fondamentale, qui n'a toujours pas été résolue au niveau scientifique, a dû se poser dès les premiers temps.
Qu'est ce que la vie ?
Au niveau fondamental, j'entends.
L'absence de vie, c'est la mort, mais quelle est la différence entre la vie et la mort ?
Au niveau de la médecine, la définition de la mort a évolué.
Il y a longtemps, un mort était quelqu'un qui ne respirait plus.
Cependant, il s'est avéré que ça ne suffisait pas.
D'abord, avec la possibilité qu'ont eu des gens de se remettre à respirer, mais surtout avec la possibilité de vivre sous respiration artificielle.
La définition a donc été de déclarer quelqu'un comme mort une fois que le cœur ne battait plus.
De la même façon, avec les massages cardiaques et surtout les cœurs artificiels, la définition a dû être revue.

Mais même dans l'antiquité, la définition n'a pas toujours été unanime.
Pour certains, une personne est morte lorsque son corps commence à se décomposer.
C'est une définition qui en vaut une autre.
Elle a cependant l'inconvénient de demander plusieurs jours d'observation avant d'apporter une réponse.
Avec la science moderne, cette définition n'est plus utilisable.
La science permet d'empêcher, ou au moins de retarder considérablement, la décomposition.
Si la médecine ne fait rien pour empêcher la décomposition, la mort peut survenir sur une personne qui aurait pu être sauvée.

Aujourd'hui, une personne est morte si aucune activité électrique n'est détectée dans son cerveau pendant un certain temps.
C'est une avancée, mais pour combien de temps ?
Comme ça ne répond pas à la question, ça risque de devenir obsolète un jour.
Le mérite de répondre, temporairement du moins, à la question : «~Quand la vie s'arrête-t-elle ?~» n'aide pas à comprendre ce qu'est la vie.
D'ailleurs, vous avez tous dû entendre parler de gens qui ont vu un tunnel.
Ils sont déclarés morts physiquement, mais ils reviennent à la vie avec une expérience plus ou moins commune.
Il y a trop de cas recensés pour être ignorée.
Il y a une histoire commune, les «~morts~» vivant cette expérience plus ou moins complètement.
Certains peuvent décrire ce qu'ils ont vu se produire dans le bloc opératoire alors qu'ils étaient morts.
Cela ne prouve rien quant à l'existence de l'âme.
Mais cela prouve que le pronostic des docteurs était faux.
Malgré tous les appareils sophistiqués, la vie n'était pas détectée.
C'est la raison pour laquelle certaines personnes essayent de s'opposer au don d'organes.

Personne n'a pu voir une âme, certains scientifiques mettent en doute son existence.
À une époque reculée, avec les phénomènes non expliqués (allant du rêve à l'hallucination en passant par des bruits bizarres), la question des esprits a dû venir d'elle même.
Si nous voyons qu'il existe des objets inanimés, comme les pierres, des êtres vivants qui peuvent mourir et qui semblent posséder quelque chose en plus du corps, il est facile d'imaginer qu'il peut exister des choses purement immatérielles.
Que les esprits soient des morts ou des êtres qui ont toujours existé ne changent rien.
Leur explication est naturelle.

Assez naturellement, vient une explication claire.
Une nature unie, avec des êtres immatériels qui agissent entre eux.
Ces êtres immatériels deviennent de plus en plus importants, surtout avec l'agriculture.
En effet, un chasseur cueilleur compte surtout sur lui même.
Il doit avoir une bonne vue pour trouver des proies, des fruits ou des céréales.
Il doit aussi être habile pour capturer sa proie une fois qu'il l'a vue.
Par contre, l'agriculteur doit espérer que ses bêtes ne tomberont pas malades et que le temps sera bon pour la récolte à venir.
Il est plus dépendant des éléments.
Ou au moins, il réalise de manière plus importante sa dépendance.
Il donne donc plus d'importance et est plus enclin à demander leur clémence.
Nous passons donc assez facilement d'une nature unie possédant des être immatériels à une nature gérée par de véritables dieux.

Il faut bien comprendre le polythéisme de l'époque comme fondamentalement différent du monothéisme actuel.
Les dieux sont des êtres supérieurs qu'il faut contenter.
Il ne sont ni bons ni mauvais, ils veulent que que les hommes leurs plaisent.
Un dieu ne va pas s'intéresser à ma relation avec mon voisin.
Sauf si mon voisin se comporte bien avec ce dieu et que mon intervention empêche mon voisin de lui plaire.
De plus, ils sont liés à leur environnement et ne sont pas en nombre limité.
Un voyageur pouvait très bien invoquer un dieu local lorsqu'il était loin de chez lui.
Cela pouvait l'aider, surtout s'il voyait que le peuple qu'il visitait était prospère.
De même, une personne pouvait invoquer le dieu d'un voyageur prospère pour le cas où.
Dans les ports, sur les routes commerçantes ou dans les ambassades, il y avait facilement des échanges de dieux.
Puis, le nationalisme est arrivé.

\section{La monolâtrie}

Je n'aime pas ce terme, mais c'est malheureusement le terme officiel.
C'est l'adoration d'un seul dieu parmi plusieurs.
Ce n'est pas le monothéisme, qui est l'adoration du seul Dieu existant.
Pour un monothéiste, seul Dieu existe, il n'y a pas de choix.
Pour un monolâtre, plusieurs dieux existent, il faut en choisir un et ignorer les autres.
C'est le premier pas vers le monothéisme.
La conception est fondamentalement différente, mais son application est proche.
Comme je l'ai dit dans la définition de l'\Index{histoire}, la \Index{Bible} n'a pas été écrite en une semaine.
Elle a été écrite sur plusieurs générations, ses premiers textes étant basés sur une tradition orale beaucoup plus ancienne.
La \Index{genèse} est surtout formée de deux textes qui ont été écrits à deux périodes très différentes.
Il est intéressant de regarder les anciens textes de la \Index{Bible} car ils montrent une conception initiale différente.
Cette conception permet de comprendre l'arrivée du monothéisme.

Dans les premiers textes bibliques, Dieu ne dit pas : «~Je suis le seul Dieu, vous n'avez pas à adorer ce qui n'existe pas.~»
Il dit : «~Je suis un dieu jaloux, vous ne devez pas adorer les autres dieux.~»
Ce qui montre (avec d'autres passages allant dans ce sens) que les premiers juifs avaient un dieu de prédilection parmi une pléthore de dieux.
Ils n'étaient pas monothéiste, mais monolâtres et sont devenus monothéistes.
Le passage se conçoit bien.
Un dieu de prédilection qui devient de plus en plus important jusqu'à devenir unique.
Une autre façon de passer au monothéisme aurait pu être de concevoir que les différents dieux ne sont que des manifestations d'un dieu unique.
Un peu comme les hindous.
Mais ce n'est pas ce qu'il s'est passé.

Pourquoi en sont-ils venus à la monolâtrie ?
Je l'ai dit à la fin de la section précédente, par nationalisme.
Ce terme peut sembler fort avec des connotations péjoratives, je dois donc l'expliquer.
Ce n'est pas très compliqué à saisir.
Un peuple peu nombreux, vagabond et attaqué de toutes parts (de l'\propre[y]{x}{Égypte} à \propre[y]{x}{Babylone} probablement) a besoin d'être solidaire.
Le besoin de solidarité est plus simple derrière un Dieu de prédilection.
La notion du bien et du mal inexistante pour des peuples polythéistes peut être importée de \propre[y]{x}{Perse} avec profit.
Si notre ennemi nous attaque et qu'en même temps, j'attaque mon coreligionnaire, c'est notre ennemi qui va gagner.
Il est donc important de nous aimer entre nous pour survivre.
Nous devons donc nous aimer derrière un dieu commun.
C'est un peu simplifié, mais c'est correct dans les grandes lignes.

Pour aider encore plus notre amour fraternel, nous pouvons rejeter les autres pour qu'ils ne viennent pas nous pervertir.
En rejetant les autres, nous rejetons leurs dieux et nous devenons monolâtres.
Notre dieu peut grandir jusqu'à devenir unique, nous sommes prêts pour le monothéisme qui peut s'implanter peu à peu.
Quand je dis «~nous~», je me mets à la place des premiers juifs, je ne parle pas de moi ou d'un personne qui aurait pu me lire.

\section{Le monothéisme}

Quand \propre[y]{Jésus}{Christ} est arrivé, il faisait partie d'un peuple fondamentalement mono\-théiste.
Ou presque, parce que les anges et démons peuvent être associés à des dieux inférieurs.
C'est probablement la façon dont la transition s'est faite.
Les dieux non initialement choisis ont pu être relégués au rang d'anges et de démons lorsque le dieu principal montait en puissance.

Maintenant que Jésus est là, il y a deux visions de cette évolution.
La première, la version officielle, c'est que \propre[y]{Jésus}{Christ} a ouvert sa religion au monde entier.
Avant lui, seuls les juifs pouvaient être sauvés.
Avec lui, tout le monde peut être sauvé.
La seconde version est soutenue par \propre[y]{Robert}{Wright} dans son excellent livre «~The evolution of God~».
Je vais simplement résumer le point qui nous intéresse ici, cela ne résume absolument pas son livre.
Je ne sais pas si sa vision des choses est vraie, mais elle est très convaincante.
En tous cas, ses arguments son bien trop complets pour êtres balayés d'un simple revers de la main.
Je ne vais pas détailler ses arguments ici, je vais juste résumer sa conclusion.

Donc, voilà sa conclusion, que tout le monde attend avec impatience (ou plutôt ceux qui me lisent et je ne dois pas être très nombreux).
Pour lui, \propre[y]{Jésus}{Christ} était un prédicateur parmi d'autres de son époque.
Pas exactement parmi d'autre, parce qu'il aurait réussit à rallier plus de monde que les autres.
Mais au niveau théologique, il n'aurait pas apporté grand chose.
Il aurait annoncé que le peuple Juif allait bientôt dominer les autres et que ceux qui suivaient ses préceptes seraient récompensés.
Ces préceptes étant la supériorité des juifs sur les autres peuples et le devoir d'aimer les autres juifs en oubliant les clivages.
Puis, il est mort, ce qui a posé le problème de son royaume.
Et c'est l'arrivée de \propre[y]{St}{Paul} qui aurait modifié cette conception.
Il aurait ouvert la religion aux non juifs.
C'est la moins discutable des conclusions, car l'abandon de la circoncision et de l'interdiction de manger de la viande de porcs sont post-\propre[y]{Jésus}{Christ}.
Mais sa principale contribution, serait la récompense.
Les élus ne feraient plus parti des dominants lorsque les juifs règneront sur le monde, mais ils seraient récompensés dans le ciel.
La raison de ce changement de conception est que \propre[y]{Jésus}{Christ} professait une domination très proche et que le temps avançant, les chrétiens mouraient sans voir d'avancée.

Quelque soit la vision réelle, pour mon niveau, ça ne change rien.
Avec le christianisme arrive un changement théologique, qu'il soit initié par \propre[y]{Jésus}{Christ} ou par \propre[y]{St}{Paul}.
Ce n'est pas un petit changement.
Les juifs connaissaient le baptême, mais c'était un rite automatique.
La religion se transmettait de manière héréditaire, comme une maladie génétique.
Avec le christianisme, la religion est choisie.
Je suis d'accord que le choix d'un enfant de trois jours est limité.
Je suis aussi d'accord que le choix entre la conversion et la tête coupée est aussi limité.
Mais si, en pratique, le choix n'est pas réel, au niveau théologique, un pas très important est fait.
C'est la première fois que le choix de la religion est posé.
Les peuples polythéistes ne choisissaient pas leur religion, ils choisissaient les dieux qu'ils adoraient, ce qui est fondamentalement différent.
Ce choix de religion est d'ailleurs le premier pas vers un autre choix, celui de ne plus croire.
Nous y reviendrons plus tard, pour l'instant, nous devons aller un peu plus loin.

\section{Retour au polythéisme}

Si vous connaissez un peu la \Index{Bible}, vous avez pu voir qu'il y a des messages différents entre les périodes.
Pour l'ancien testament, il y a clairement des messages de guerre et de destruction totale des ennemis.
Par contre, dans le nouveau testament, en dehors de l'apocalypse (qui est une description, pas une prescription), il n'y a plus de message de violence.
Il n'y a que de l'amour qui dégouline dans tous les sens.
Il y a donc une explication à trouver pour le retour à la violence.
La dominance de la religion aide à expliquer ce retour.
Si vous êtes peu nombreux et entourés d'ennemis, vous avez intérêt à vous aider entre vous pour vous protéger des ennemis.
Vous avez aussi intérêt à être gentils avec vos ennemis pour ne pas vous faire exterminer.
Le choix n'est pas forcément calculé, mais si vous avez fait d'autres choix, vous avez disparu et plus personne n'est là pour subir votre influence.
Ce sont donc ces choix qui ont été faits, même si les raisons étaient autres.
Une fois que vous êtes très majoritaires, vous pouvez commencer à vouloir être agressif envers vos ennemis.
Mais si la paix avec vos ennemis a été profitable et vous a permis de vous développer, il n'y a aucune raison de changer.

Il y a donc eu besoin d'un apport extérieur.
C'est la chevalerie.
Vous ne voyez pas le rapport entre la chevalerie et le polythéisme ?
C'est pourtant simple, la chevalerie, est la christianisation de légendes celtes.
L'histoire des chevaliers de la table ronde se base sur la légende du roi \propre[y]{x}{Arthur}.
Cette légende regroupe à la fois des faits historiques et des légendes beaucoup plus anciennes.
Le personnage historique était probablement un chef de guerre local qui aurait ralenti l'invasion germanique à la fin de l'empire romain.
Ses faits ont été exagérés, comme toujours avec les légendes.
Par dessus, des traditions plus anciennes se sont greffées et nous avons les chevaliers de la table ronde.
Cette tradition transmise oralement a été écrite par des moines, qui ont, bien entendu, christianisé les héros.
La chevalerie est donc l'alliance entre la bravoure celte et l'amour chrétien.

Une fois que la justification de la violence pour la bonne cause est établie, les dérives peuvent commencer.
Ils commencent par combattre quelqu'un pour protéger la veuve et l'orphelin.
Puis ils combattent des gens qui pourraient attaquer la veuve et l'orphelin.
Puis des gens qui sont indifférents à la veuve et à l'orphelin.
C'est pour la bonne cause, la plus grosse dérive étant de justifier que c'est pour empêcher la personne d'aller en enfer.
L'amour est toujours là, du moins en théorie, mais n'est plus tout le temps simple à trouver.
La bravoure n'est plus facile à trouver non plus, torturer quelqu'un attaché n'est pas un acte très brave.
Mais ce sont quand même les deux éléments fondateurs de la chevalerie.

\section{Et maintenant ?}

Comme je le disais à la fin de la section sur le monothéisme, le choix de la religion apporte une réflexion.
Pour choisir, il faut se demander si la religion choisie est en adéquation avec notre pensée.
Avec le temps la pensée évolue à deux niveaux.
Les gens aiment suivre les préceptes de la \Index{Bible} qui appelle un monde meilleur.
Même s'il y a des appels à la guerre et à la destruction des ennemis, ces passages ont été oubliés par la majorité des chrétiens.
La \Index{Bible} est considérée comme un livre d'amour par le plus grand nombre.
En même temps, les dérives de l'église sont de plus en plus abjectes aux yeux du plus grand nombre.
Que ce soit pour l'invasion de l'\propre[y]{x}{Amérique}, les guerres de religions ou le refus de la condamnation du nazisme pendant la seconde guerre mondiale.
Le second niveau est l'évolution de la science, qui montre de plus en plus que certaines affirmations de la \Index{Bible} sont fausses.
Face à cette évolution des mentalités, trois possibilités sont offertes~:
\begin{tuxliste}
\item rejeter la science et considérer que la \Index{Bible} fait foi~;
\item adapter la conception de \propre[y]{x}{Dieu} et mettre la lecture de la \Index{Bible} à un second niveau~;
\item rejeter l'existence de \propre[y]{x}{Dieu}.
\end{tuxliste}

La première possibilité ne tient pas la route, elle est peu suivie.
Rejeter la science en disant que c'est une croyance comme une autre, n'est pas suivie par ceux qui le font.
Ces personnes ne rejettent de la science que ce qui est en contradiction avec la \Index{Bible}.
Or, la science forme un tout.
Pour que ces personnes soient crédibles, il faudrait qu'elles rejettent la médecine, l'avion, le téléphone, les ordinateurs la télévision et tous les moyens modernes qui n'existent que grace à la science.
Ce sont des fanatiques, il n'y a pas de discussion possible avec eux.

La seconde possibilité est déjà plus crédible.
\propre[y]{x}{Dieu} n'est plus considéré comme un être dans les nuages presque palpable.
Il est considéré de façon beaucoup plus abstraite, et assez mal définie.
C'est d'ailleurs une critique faite par les scientifiques qui le rejettent.
Probablement personne, surtout parmi les scientifiques quantiques, ne comprend vraiment ce qu'est un électron.
Comme le disait à peu près un scientifique dont j'ai oublié le nom, \emph{ceux qui ont une vision claire de ce qu'est un électron ne savent pas ce qu'est un électron}.
Un électron, cette petite particule à la base de tout l'univers, est à la fois une onde (donc immatériel) et un objet physique (donc matériel).
C'est une particule qui se comporte différemment si elle est observée ou pas.
Il faut avouer que c'est difficile à concevoir.
Est-il honteux d'avoir du mal à concevoir \propre[y]{x}{Dieu} qui représente la totalité de ce qui existe alors qu'il est dur d'arriver à ne concevoir qu'une particule presque élémentaire ?
J'ai tendance à dire que non.
Pour expliquer les catastrophes (naturelles ou pas), l'interaction entre \propre[y]{x}{Dieu} et le monde varie en fonction des personnes.

La troisième possibilité est tout aussi crédible.
Pour l'instant, la science ne sait pas démontrer si \propre[y]{x}{Dieu} existe ou pas.
Tout ce que les scientifiques savent, c'est que son existence n'est pas une hypothèse utile pour les démonstrations.
Tout ce que nous voyons autour de nous peut être expliqué, soit comme une intervention divine, soit comme du hasard.
Le choix est donné à chacun de choisir.
Bien sûr, choisir l'intervention divine dès que c'est bien et le hazard dans le cas contraire n'est pas très crédible non plus.

\section{Pourquoi la religion ?}

C'est une question qui peut se poser.
Si \propre[y]{x}{Dieu} n'existe pas, pourquoi les gens y croient-ils ?
Qu'une personne croit en \propre[y]{x}{Dieu}, c'est une chose.
Mais que les gens croient en \propre[y]{x}{Dieu} d'une manière globale dans l'ensemble du monde porte à réflexion.
Si \propre[y]{x}{Dieu} existe, la question se pose aussi.
Pourquoi aurait-il fait évoluer les animaux pour qu'ils se transforment en hommes qui croient en lui de façon assez naturelle ?
Son existence ou sa non-existence ne change pas la question initiale.
Vous devez voir où je veux en venir.
Est-ce que comme le considérait \propre[y]{Karl}{Marx}, la religion bénéficie aux puissants aux dépends du peuple ?
Ou est-ce que la religion est aussi bénéfique pour le peuple ?
Bref, la religion, indépendamment de l'existence de \propre[y]{x}{Dieu}, est-elle vouée à disparaître ou à perdurer ?

La religion (au sens large, j'englobe le chamanisme) est bénéfique pour les dirigeants.
Il n'y a pas de doute à ce niveau là.
L'église catholique s'est considérablement enrichie, mais les sorciers, chamanes et prêtres des différentes religions ont toujours eu des avantages.
Pour le peuple, les bienfaits de la religion sont plus subtils.
Pour que le prêtre puisse avoir beaucoup d'argent, il faut que le peuple gagne de l'argent.
Si le peuple se fait la guerre, il ne cultive pas son sol, et ne peut pas faire de récolte.
Si les paysans se brûlent les champs les uns des autres, le problème sera identique.
Donc, les religions demandent une certaine cohésion au sein du groupe.
L'avantage de la religion pour les personnes est donc une certaine sérénité.
Ils ont moins à s'inquiéter du comportement de leurs voisins.
De même, les prêtres apportent des réponses réconfortantes (qu'elles soient justes ou pas ne change rien) à leurs ouailles.
La religion s'intègre bien dans un processus d'évolution.
Le \Index{meme} de la religion permet de comprendre pourquoi les religions se sont développées et comment elles en sont arrivées là.

Mais alors, l'athéisme qui se développe ne contredit pas cet apport de la religion ?
L'athéisme se développe majoritairement dans les milieux scientifiques.
L'hypothèse de \propre[y]{x}{Dieu} en science est néfaste.
Le problème est que cette hypothèse peut répondre à toutes les questions.
Dès qu'il y a une difficulté, l'utilisation de \propre[y]{x}{Dieu} comme réponse arrête la recherche.
Il faut donc refuser cette hypothèse pour faire avancer la science.
Le côté néfaste de \propre[y]{x}{Dieu} pour l'avancée de la science a tendance a diminuer la foi de certains scientifiques.
Pour que la société puisse évoluer, il est donc nécessaire de concilier ce côté néfaste de la vision de \propre[y]{x}{Dieu} dans la science avec l'apport de la religion à la société.
La morale judéo-chrétienne est bien intégrée à notre culture, indépendamment de la religion, maintenant.
La religion n'est plus utile au niveau cohésion sociale, en s'intégrant à notre culture, elle a joué son rôle.

Pour l'avenir de la religion dans le long terme, il y a deux points qui sont à prendre en compte.
Le premier est l'avancée de la science.
Il est possible que dans un futur lointain la science finisse par démontrer si \propre[y]{x}{Dieu} existe ou pas.
Si cette démonstration est faite, il y aura un impact certain sur l'avenir des religions.

Le second point est psychologique, je ne suis pas capable de l'étudier.
Les gens ont besoin, dans certaines mesures, de guides, de cadres pour leurs actions.
Aujourd'hui, la société a tendance à remplacer la religion.
La loi oblige les gens à faire ce qui va dans le sens de la société (dans une large mesure, je ne décris pas le pays des bisounours ici), alors que la religion les incites à le faire pour des raisons avouées différentes.
Le psychologue remplace le prêtre qui servait de confesseur.
L'avenir nous dira si ce que la société propose à la place de la religion conviendra aux croyants.

\section{Digression nécessaire}

Il y a un point que je voudrais aborder, c'est le rejet de la religion.
Rejeter la religion parce que la personne ne croit pas en \propre[y]{x}{Dieu} est une attitude naturelle.
Par contre, interdire aux autres leurs religions est une autre attitude.
Certains disent que les religions ont fait du mal par le passé.
C'est vrai, c'étaient des attitudes inexcusables.
Mais interdire aux gens de croire risque d'amener aux mêmes travers.
Dans l'autre sens, cette fois.
\propre[y]{Adolphe}{Hitler} et \propre[y]{Joseph}{Staline} ont prouvé qu'il n'y avait pas besoin de brandir la religion pour fanatiser les foules et amener des exactions terribles.

Vouloir prouver que les religions sont mauvaises en elles-mêmes est assez délicat.
Il y a des appels à la guerre dans la \Index{Bible} et dans le \Index{Coran}, c'est vrai.
Mais s'arrêter là est un peu simpliste.
Il faut essayer de se replonger dans le contexte pour essayer de comprendre ces textes.
Est-ce que quelqu'un irait, aujourd'hui, tuer des Anglais sous prétexte de suivre les préceptes de \propre[y]{Jeanne d'}{Arc} ?
Non, bien sûr.
Pourquoi ?
L'explication est naturelle, la \propre[n]{x}{France} était en partie envahie.
C'est dans ce contexte, sous-entendu, qu'elle s'exprimait.
De même que les généraux, qui ont exhorté les soldats à tuer des Allemands pendant les guerres mondiales, ne conseilleraient pas ces actions aujourd'hui.
Le contexte n'est plus le même.
Pour encourager les troupes avant un assault, un général ne peux pas dire : «~Tuez tous les Anglais que vous pouvez, mais seulement pendant cet assault, après, nous verrons ce que nous ferons en fonction des circonstances, je vous le dirai.~»
Vous comprenez pourquoi, l'ordre doit être simple, clair, rapide à prononcer.
Une fois prononcé, il ne faut pas que les soldats en aient oublié le début.
Un ordre tel que : «~Chacun d'entre vous doit me rapporter au moins cinquante scalps d'Anglais !~» est beaucoup plus efficace.
Mais si, l'assaut terminé, une paix est signée, un soldat n'ayant rapporté que dix scalps ne va pas continuer sa guerre dans son coin pour rapporter les quarante scalps manquant.
L'ordre était donné dans un contexte et interprété tel quel.

La \Index{Bible} et le \Index{Coran} ont aussi été écrits dans certains contextes.
Avant d'interpréter leurs textes, il faut étudier ce contexte.
Ils ont aussi été écrits en temps de guerre, il faut donc chercher si leurs exhortations étaient limités au contexte particulier ou étaient données dans un cadre plus général.
Avant d'être trop catégorique quand à leur contenu fondamentalement mauvais, il est bon de lire ce qui est autour pour avoir une idée de la conclusion qui s'impose.
Je suppose que vous savez ce que j'en pense, je vous laisse lire les passages cités par les fanatiques anti-religion pour vous faire votre propre idée.
Ce sont les mêmes passages que ceux prônés par les fanatiques religieux pour justifier leurs guerres.
C'est simple, ils sont coupés de la même méthodes par les fanatiques de ces deux bords.





