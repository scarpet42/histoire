%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : femme.tex
% 	Modif : ven. 11 oct. 2013 15:05
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\Chapitre{La vision de la femme dans la société}

\citation{\propre{Henri}{Kissinger}}{Nobody will ever win the Battle of the Sexes. There's just too much fraternising with the enemy.}{Personne ne gagnera jamais la guerre des sexes. Il y a juste trop de fraternisation avec l'ennemi.}

\section{Prémices}

La femme a-t-elle une âme ?

\vspace{\baselineskip}

Il vous paraît scandaleux de poser la question ?
Pourquoi ?
Si je demande si l'homme a une âme, vous allez me donner votre opinion.
Vous allez me donner votre conception et, en fonction des arguments, un débat intéressant peut se lever.
Ce débat va porter sur beaucoup de sujets pour répondre de façon plus ou moins tranchée à la question.
Par contre, je mettrais assez facilement votre main à couper que la question initiale apportera un tout autre débat.
Certains vont se braquer, me traiter de macho et refuser de continuer.
Les autres vont se scandaliser et dire que l'homme et la femme sont pareils, sauf pour les rapports sexuels mais ça ne compte pas.
Bref, un débat qui pourrait même être intéressant peut se lever, mais qui n'aura aucun rapport avec la question initiale.
Donc, pour savoir si la femme a une âme, il faut forcément demander si l'homme a une âme.
Ceux qui considèrent que l'homme et la femme sont égaux ne considèrent pas que leurs noms sont interchangeables pour parler de sujets qui touchent indifféremment les deux.
Vu sous cet angle, ça peut sembler bizarre, mais c'est malheureusement le cas.
Ceux qui veulent absolument trouver un nom féminin pour tous les noms de métiers et de fonctions ne chercheraient pas un neutre plutôt ?
Parce que si je demande si la femme a une âme, vous supposez que je considère que l'homme a une âme et qu'il pourrait en aller différemment pour la femme.

La perception de la différence entre les hommes et les femmes est donc bien ancrée dans les mentalités.
Bien plus que les différences physiques peuvent laisser supposer.
C'est vraiment à cette perception que je vais m'intéresser ici.
Je ne vais pas chercher à savoir si le femme est plus adaptée que l'homme pour s'occuper du repassage ou pour conduire un char de combat.
Je vais m'intéresser à l'évolution de la place des femmes dans la société.
Je vais chercher la justification de la place de la femme.
Je ne vais pas chercher à savoir si cette justification est valide.
Je vais simplement chercher si cette justification a été acceptée de manière générale à une époque.

Avant de pouvoir étudier sérieusement la question, il est nécessaire de remarquer deux différences entre les hommes et les femmes.
Ce sont ces différences qui vont modifier les mentalités et aider leur acceptation~:
\begin{tuxliste}
\item le poids~;
\item la grossesse.
\end{tuxliste}

La première différence n'est pas toujours bien admise, je vais donc la définir de manière précise.
Il faut bien comprendre la définition et l'implication de cette différence.
De manière statistique, l'homme est plus grand et plus lourd que la femme.
Je ne le préciserai plus ensuite parce que ça alourdirait considérablement mon texte (oui, encore plus que ce qu'il est déjà).
Quand je dirais que l'homme est plus lourd que la femme, ce sera toujours d'une manière statistique, ce sera implicite.
Si vous choisissez bien, vous allez trouver une femme qui sera plus lourde qu'un homme.
Mais, si vous prenez un homme et une femme au hasard, en général, l'homme sera plus lourd que la femme.

C'est à dire que vous pouvez prendre un groupe de \Nombre{1000} hommes et un groupe de \Nombre{1000} femmes que vous pesez.
Je sais que ça prend du temps de faire \Nombre{2000} pesées, mais l'étude doit être sérieuse.
Pour que l'étude soit sérieuse, il faut aussi que les hommes et les femmes soient choisis dans les mêmes conditions.
Par exemple, si vous pesez \Nombre{1000} femmes dans un centre de cure amaigrissantes américaine et \Nombre{1000} hommes qui sont dans un camp de réfugiés depuis plusieurs mois, vous aurez d'autres conclusions.
Vous additionnez le poids des hommes, le poids des femmes et vous divisez les deux sommes par \Nombre{1000}.
Vous aurez le poids d'un homme moyen et le poids d'une femme moyenne.
Et là, si vous avez pris \Nombre{2000} personnes vraiment au hasard, vous avez en général un peu plus de \Unite{10}{kg} d'écart.

\section{Qui c'est qui commande ?}

Mesurer l'écart de poids peut sembler être une perte de temps, surtout que des mesures ont déjà été faites.
C'est d'ailleurs pour ça que je dis que l'écart est d'un peu plus de \Unite{10}{kg}.
Parce que les statistiques sont disponibles sur Internet et ce sont les conclusions qui ont été faites.
Dans les films, vous voyez \propre[n]{Angelina}{Jolie} ou \propre[n]{Cameron}{Diaz} qui se battent contre des hommes musclés et très entraînés.
Ce n'est juste pas crédible.
Entre une femme souple de \Unite{45}{kg} toute mouillée et une montagne de \Unite{100}{kg} de muscles, le combat ne peut pas se passer comme dans les films.
Pour que la femme puisse gagner, il faut qu'elle frappe très souvent l'homme sans se faire toucher.
Ou alors, qu'elle ne frappe qu'une fois à un endroit sensible, comme le nez, la gorge ou les testicules (vous l'attendiez en premier, c'est pour ça que je l'ai mis en dernier).
Mais une fois qu'elle a touché une zone sensible, le combat s'arrête et ce n'est plus comme dans les films.
Il est possible qu'une femme gagne un combat contre un homme en donnant autant de coups qu'elle en reçoit, mais pas avec le gabarit d'une actrice de cinéma contre un homme musclé.
C'est pour ça qu'il y a des compétitions en fonction du poids dans presque tous les sports de combats.
Parce qu'à niveau équivalent, le poids est discriminant.

La conclusion de tout ça, c'est que comme l'homme est plus lourd, il est plus fort et il peut imposer sa façon de voir à la femme.
Cependant, comme je l'ai montré au début, les différences sont ancrées en nous de manière profonde.
Et pas seulement chez les hommes qui ont envie d'y croire, aussi chez les femmes qui n'ont aucun intérêt à y croire.
La force n'explique donc pas tout. 
Je peux vous obliger à me dire que je suis intelligent.
Surtout si vous êtes attaché et que j'ai des outils bien choisis à ma disposition.
Par contre, je ne peux pas vous obliger à croire que je suis intelligent avec les mêmes arguments.
Ce serait plutôt l'effet inverse qui serait obtenu.
Mais la force peut aider, ça s'est peut-être déjà produit.

\section{La reproduction}

La seconde différence ne pose de problème à personne et n'est pas polémique.
Pour la reproduction humaine, il faut un homme et une femme.
Ensuite, la femme est enceinte.
Pendant le début de la grossesse, ça ne change rien, c'est au bout de plusieurs mois que la différence est importante.
Dans notre vie assistée par la voiture et les instruments électriques la femme enceinte est gênée pour faire tout ce qu'elle veut vers la fin de sa grossesse.
Pour comprendre ce qui est ancré au plus profond de notre culture, il faut remonter le plus loin possible dans le temps.
Vous imaginez une femme, enceinte de huit mois, qui devrait courir après une gazelle pour manger ?
Et ensuite, vous l'imaginez qui devrait courir pour échapper à un lion ?

Une fois qu'elle a accouché, vous croyez que c'est fini ?
Pourtant, le nouveau né a besoin d'assistance pour survivre.
Qui peut s'en occuper ?
Le père ?
Non.
Même en supposant que le père soit connu (nous en reparlerons plus tard).
Ce n'est pas une vue machiste qui renie 200 millions d'années d'évolution.
C'est une vue pragmatique qui prend en compte les conditions de l'époque.
Aujourd'hui, pour nourrir votre enfant, vous allez acheter du lait en poudre en grande surface.
Si vous voulez donner du lait maternel à votre enfant, la mère peut extraire son lait et le mettre au congélateur en attendant l'heure du biberon.
Vous pouvez aussi choisir de lui donner le sein, bien sûr, mais c'est vraiment un choix récent.
Aujourd'hui, la nourriture du nouveau né n'est qu'une question d'organisation entre l'homme et la femme.
Mais à cette époque là, il n'y avait pas le choix.
La femme devait nourrir son bébé ou le confier à une autre femme pour qu'elle le nourrisse.
Elle ne pouvait pas le laisser au père ou à un autre homme.
Comme la femme devait nourrir le bébé, il était rapide de convenir que c'est elle qui devait s'en occuper entièrement.

Donc, les femmes primitives avaient besoin de la protection des hommes lors de leur grossesses et après l'accouchement.
Protection au sens large. 
Pour se défendre des prédateurs, des clans rivaux et pour continuer à se nourrir.
De là à ce que les hommes protègent constamment les femmes, il y a une habitude à prendre.
Mais pour quelles raisons auraient-ils protégé les femmes ?
Quel est leur intérêt de perdre une partie de la nourriture durement trouvée pour la donner à une femme ?
Les hommes mariés que vous connaissez doivent avoir l'habitude de partager leur nourriture avec leurs femmes et leurs enfants.
C'est une réaction qui semble naturelle, l'inverse serait choquant.
Mais ils invitent rarement des femmes inconnues chez eux pour les nourrir (enfin je suppose que c'est pareil autour de vous).
C'est aussi une réaction qui semble normale.
Un homme qui ferait entrer une inconnue chez lui pour lui donner à manger sans autre raison que de savoir qu'elle a faim devrait trouver une explication à donner à sa femme.
Le don de la nourriture aux femmes et aux enfants se fait donc de manière sélective.
Le mariage à l'église est une invention récente.
Pour que cette sélection soit aussi ancrée en nous, c'est qu'elle est très ancienne.
Je dois donc chercher à connaître les motivations primitives et je répète ma question pour ceux qui auraient perdu le fil.

Quel est l'intérêt pour un homme de perdre une partie de la nourriture durement trouvée pour la donner à une femme ?
Pour un peu de plaisir sexuel ?
Au moment où la femme n'a pas besoin de lui, c'est une explication très crédible.
Mais si la femme est enceinte de huit mois, elle n'est pas forcément intéressée ou intéressante pour donner du plaisir sexuel et il faut une autre explication.
D'abord, le but d'un être vivant est de se reproduire et de transmettre ses gênes.
C'est ancré au fond de nous, beaucoup d'animaux font la même chose.
C'est trop ancré pour que je puisse en trouver la raison, c'est trop ancien.

Puisque je suis d'accord avec moi-même pour admettre que les hommes primitifs voulaient transmettre leur gênes, je peux partir de là.
Je ne dis pas qu'ils savaient ce que c'était des gênes, juste qu'ils voulaient les transmettre, même inconsciemment.
Vouloir transmettre ses gênes, aide à vouloir aider ses enfants.
Au moins au début, avant qu'une rivalité naisse.

Mais qui étaient leurs enfants ?
Pour la femme c'est facile, elle sait de qui elle accouche.
Mais elle ne connaît pas forcément le père.
Dans nos sociétés ultramodernes il arrive que le père puisse avoir des doutes sur ses enfants.
Même avec le mariage et les caméras vidéos pour espionner la femme.
Imaginez à quel point ça devait être dur pour les premiers hommes.
Je ne parle pas de la blonde qui accouche de jumeaux et qui ne sais pas qui est le père du second.

Non, je parle de sociétés primitives (au vrai sens du terme, pas au sens de contemporaine moins avancée technologiquement).
Dans les sociétés primitives, ils ne connaissaient pas les mécanismes de reproduction.
Ils connaissaient les plaisirs sexuels, c'est normal.
Mais il y a neuf mois d'écart entre l'acte et l'accouchement.
Il est possible de réduire l'écart, il y a environ trois mois entre l'acte et le moment où la grossesse devient visible.
Il est donc loin d'être instantané de connecter les deux événements.

\section{Vive les femmes}

Le premier indice, qui a dû se voir, c'est que les femmes qui avaient des rapports sexuels avec les hommes avaient plus d'enfants que celles qui n'avaient pas de rapport sexuel.
Cela a permis de savoir que pour l'enfantement, la grossesse nécessitait l'intervention de l'homme.
Sans connaître l'importance de l'homme.
Par contre l'importance de la femme était flagrant.
C'est donc tout naturellement que les femmes ont été considérées comme les élément centraux des sociétés primitives.

Nous n'en avons plus de trace dans notre culture.
Mais les statuettes primitives représentant des femmes sont nombreuses.
La place primordiale de la femme chez les premiers habitant du territoire qui est actuellement la France se retrouve dans la déesse \Index{Celte} \Index{Birgidh}.
Si le nom de cette déesse vous semble familier, c'est normal, c'est du nom de cette déesse que vient le prénom Brigitte.
C'est probablement la déesse primordiale qui était tellement importante que lorsque les Celtes sont arrivés, ils l'ont inclue dans leur panthéon.
De même, la déesse \Index{Gaïa}, qui représente la terre et l'union de tous les êtres vivants vient probablement des habitants de \propre[y]{x}{Grèce} pré \Index{indo-européen}s.

C'est donc la vision des sociétés primitives avec leurs premières observations.
La femme symbolise la vie, elle est importante.
Elle doit être protégée et nourrie.
C'est la vision des premières sociétés de chasseurs cueilleurs.
Cette vision a durée longtemps, puis avec les sociétés agricoles, les observations de la nature ont changées.
La place de la femme aussi.

\section{Le bouleversement de l'agriculture}

Avec l'apparition de l'\Index{agriculture}, la vision a changée.
L'agriculteur remarque que quand il plante une graine, le sol est important.
Si la graine est posée sur une pierre, elle ne donnera jamais rien, elles a besoin d'être dans le sol.
Si le sol est riche, bien arrosé, bien entretenu, la plante se développera mieux que dans un sol sec, dur et pauvre.
Mais ce qu'il remarque surtout, c'est que le sol ne modifie pas les propriétés fondamentales de la plante.
S'il change l'arbre de sol, l'arbre sera plus ou moins beau, mais il sera identique.

L'analogie est rapide.
Le sperme de l'homme est la graine et le ventre de la femme le sol.
Tout à coup, les rôles sont inversés.
Pour que l'enfant soit beau, grand, fort et intelligent, il faut que l'homme soit beau, grand, fort et intelligent.
Une femme sera importante pour que l'enfant puisse naître, mais elles deviennent interchangeables.
C'est le meilleur homme qui donnera les meilleurs enfants, tout est dit.
Une petite remarque pour voir que l'hérédité des fonctions (métiers, charges, castes et autres) vient assez naturellement à l'esprit.

Avec l'\Index{agriculture}, la force devient importante.
La protection du sol travaillé se fait par la force.
Le travail du sol se fait par la force.
Combiné avec la vision de l'hérédité, la femme est définitivement reléguée à un rôle secondaire dans la société.
Une place qui est récente dans l'histoire de l'humanité, environ \Unite{12 000}{ans}, sur plus de \Unite{150 000}{ans}, mais largement suffisante pour continuer à marquer les esprits aujourd'hui.

\section{La science au secours de la femme}

Les observations de la science ont évoluées.
Avec les microscopes et les analyses des gènes, la vision de la reproduction a fondamentalement changée.
Maintenant, tout le monde sait que la femme fourni la moitié des caractéristiques de l'enfant, l'homme en fourni l'autre moitié.
Pour que l'enfant soit beau, intelligent et musclé, la femme a autant d'importance que l'homme, elle n'est plus interchangeable.
Mais sa place dans la société n'a pas changé pas pour autant avec cette simple constatation.

Avant le changement de la reproduction, la science a eu un autre impact.
Au début du chapitre, j'ai parlé de la différence de force.
la science, appliquée à la technologie, a permis de diminuer l'importance de l'écart des forces.
Pour transporter un poids sur une grande distance, les hommes primitifs avaient besoin de toute leur force.
Avec la domestication, la force est devenue moins importante, mais elle est restée importante.
La force devient nécessaire pour maîtriser l'animal, qui lui doit tirer les lourdes charges.
Avec la révolution industrielle, la machine a vapeur a permis de rendre la force inutile pour les travaux les plus extrêmes.

Par exemple, lors de la construction des dolmens, il a fallu découper des gros blocs de pierre et les transporter sur des longues distances.
Aujourd'hui, pour tailler un bloc de pierre, il est possible de prendre un tracto-pelle.
Ensuite, avec une grue, il est possible de mettre la pierre dans une benne.
Enfin, le camion transporte la pierre sur une longue distance.
Ce sont des outils qu'il faut apprendre à maîtriser, mais dont la force requise pour leur maîtrise est sans commune mesure avec la force utilisée pendant le paléolithique.

Grace à la science, dans pratiquement tous les secteurs de la vie, la femme peut faire le même travail que l'homme.
Bien sûr, certains métiers restent physiques et toutes les femmes ne peuvent pas faire tous les métiers.
Mais depuis quelques années, les femmes les plus robustes peuvent faire tous les métiers.

Cependant, entre les apports Cependant, ces apports n'ont pas été suffisants pour changer la place de la femme dans la société.
Il y a eu un gros délai entre le moment où la femme a eu la capacité physique pour occuper d'autres postes et le moment où elle les a effectivement occupé.
Tant que les mentalités étaient ce qu'elles étaient, les avancées technologiques n'ont pas modifié les mentalités.
L'élément déclencheur a été la première guerre mondiale.
Lorsque tous les hommes valides étaient au front, il fallait continuer à faire tourner le pays.
La guerre devait durer quelques jours, les usines et les champs pouvaient attendre.
Mais quelques années sont trop longues pour se permettre d'attendre.
Les femmes ont commencé à remplacer les hommes avec succès dans certains postes.

À la fin de la guerre, les hommes ont pu reprendre leur place.
Ou plutôt, ceux qui n'étaient ni morts ni blessés.
Certaines femmes sont donc restées à certains postes.
Le changement de mentalité était en marche.

Avec le lait en poudre et la possibilité pour les femmes de tirer leur lait pour le mettre au réfrigérateur, les hommes peuvent s'occuper de leurs enfants dès le plus bas âge.
La gestion des enfants et du métier n'est plus qu'une question d'organisation au sein du couple.
Ce n'est pas que l'organisation est toujours facile, mais qu'elle est possible.
La seule différence aujourd'hui entre l'homme et la femme dans la société est pour la grossesse.
C'est quelque chose que seule la femme est actuellement capable de faire.

\section{Bilan}

Il ne faut pas dire que la femme est l'égale de l'homme, parce que ça ne veut pas dire grand chose.
Dire qu'à travail égal le salaire doit être égal est une revendication légitime.
Pendant longtemps, les hommes et les femmes ne faisaient pas vraiment le même travail.
Dans les bureaux, l'efficacité était la même, mais la femme faisait généralement moins d'heures pour aller amener et récupérer les enfants à l'école.
C'était généralement la femme qui ne travaillait pas le mercredi pour s'occuper des enfants.
Cette organisation est encore générale, mais de plus en plus de pères s'organisent avec leurs femmes pour s'occuper des enfants en dehors des heures d'école.

Il faut surtout comprendre que s'il y a effectivement des différences entre l'homme et la femme, ça ne veut pas dire que l'un est mieux que l'autre.
Quand je mange, j'utilise un couteau et une fourchette.
Je ne me pose pas la question de savoir si mon couteau est mieux ou moins bien que ma fourchette.
Je ne cherche pas non plus à manger avec deux couteaux ou deux fourchettes, je sais que ce sera moins pratique.



